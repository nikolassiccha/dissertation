% !TEX root = ../../main.tex
For constant Poisson brackets, just like for linear hyperbolic systems
of balance laws, many things simplify. Nevertheless, they have attracted
some interest over the years, mainly in the context of multi-Hamiltonian
systems, see e.g.~\cite{Gumr1990}. Olver and Nutku examined one
particular class of one-dimensional, \({2} \times {2}\) constant
hydrodynamic Poisson brackets in \cite{Olve1988}, following
\cite{Nutk1985} and \cite{Nutk1987}. The same class has been connected
to the shallow water equations in \cite{Cava1982}, to Poisson's equation
in \cite{Nutk1987}, to the Born-Infeld equation in \cite{Arik1989} and
to Riemann invariants in \cite{Neyz1989}. This class has been further
investigated in \cite{Carb1995}. Benjamin and Bowman considered
discontinuous solutions of general one-dimensional constant Poisson
brackets in \cite{Benj1987}.

Here, we will only state some immediate results connecting constant
hydrodynamic Poisson brackets to (possibly hyperbolic) systems of
quasilinear first-order PDEs with an additional conservation law.

We adapt the customary definition of constant Poisson brackets on finite
dimensional vector spaces as given e.g.~in \cite{Laur2013}, definition
6.1:
\begin{definition}[constant]\label{definition34d8f06e68a42a6cda5d246bce560f73}
A hydrodynamic Poisson bracket \(\pi\) on the infinite-dimensional
function space \(C^\infty\left(\mathcal{M}^d; \mathcal{U}^n\right)\) is
called \emph{constant}, if for each pair of linear functionals F and G
of the form
\({I} = {\int {{f}_{i}\left(x\right)} {{u}^{i}_{}\left(x\right)}\, dx}\),
their Poisson bracket \(\pi\left(F, G\right)\) is a constant function on
\(C^\infty\left(\mathcal{M}^d; \mathcal{U}^n\right)\).\end{definition}

This translates to the equivalent conditions
\({{{g}^{\mu}_{}} = {{\mathrm{symm}} {\mathrm{const}}}} \text{ and } {{{b}^{\mu}_{}} = {0}}\),
hence we have as evolution equation
\begin{align}\label{eq89581bb29bbd0c036d8a1753baa85b69}
{u}^{}_{t} = {{g}^{\mu}_{}}   {{\nabla} ^ {2}}   {h}   {{u}^{}_{\mu}}.\end{align}
As can be immediately verified from
(\ref{relationsc8f5eee804ce8db284850ef06a183d08}), any constant
skew-symmetric bracket is a Poisson bracket. Furthermore, from lemma
\ref{lemma97924dafd18947a120b4f809fceaa333} we get:
\begin{corollary}\label{corollaryfa4607653c4959e03a965ab9bae6c43f} Any
\hyperref[definition34d8f06e68a42a6cda5d246bce560f73]{constant}
hydrodynamic Poisson bracket induces a system of conservation laws with
an additional balance law for the energy density. If the energy density
is strictly convex, the system is symmetrizable. \end{corollary}

\begin{proof}\label{corollaryfa4607653c4959e03a965ab9bae6c43f} Follows
from lemma \ref{lemma97924dafd18947a120b4f809fceaa333} and
\({{b}^{\mu}_{}} = {0}\).\end{proof}

\begin{corollary}\label{corollary16816a6dc02cd509f88e23994bce8eb1} In
one spatial dimension (\(d=1\)), \emph{all} constant hydrodynamic
Poisson brackets with positive definite matrix \(g\) induce a
symmetrizable system of conservation laws for \emph{any} hydrodynamic
Hamiltonian.\end{corollary}

\begin{remark}\label{corollary16816a6dc02cd509f88e23994bce8eb1}

\begin{itemize}
\tightlist
\item
  The \hyperref[definition5560523c9b90d17693db78f3deb39fb1]{symmetrizer}
  is \({g}^{-1}\)
\item
  For \(d > 1\) the system is generally not symmetrizable.
\end{itemize}

\end{remark}

\begin{corollary}\label{corollarya664c335c17a8b9d127c5ea17e2e8da0}
\emph{Any} linear system of first-order PDEs
\begin{align}\label{eq7ce433a3c6d539e83f219fd48fcabcdd}

        {u}^{}_{t} + {{A}^{\mu}_{}}   {{u}^{}_{\mu}}& = 0
\end{align} that admits an additional conservation law for
\(\eta\left(u\right)\) with non-singular and constant Hessian can be
written using a constant hydrodynamic Poisson bracket with
\({{g}^{\mu}_{}} = {{{A}^{\mu}_{}} {{\nabla} ^ {-2}} {\eta}}\).\end{corollary}

\begin{proof}\label{corollarya664c335c17a8b9d127c5ea17e2e8da0}

From the existence of a conservation law for \(\eta\left(u\right)\) we
get from the integrability condition
(cf.~(\ref{eq84c0bedaad223970935fc47d0893d1fc}))
\begin{align}\label{eq080fcde17e2547f40713c9d10fc1f2ac}
{{\nabla} ^ {2}}   {\eta}   {{A}^{\mu}_{}} = \left({{\nabla} ^ {2}}   {\eta}   {{A}^{\mu}_{}}\right)^T = {\left({A}^{\mu}_{}\right)^T}   {{\nabla} ^ {2}}   {\eta}\end{align}
and hence after multiplication with \({{\nabla} ^ {-2}} {\eta}\) from
the left and from the right
\begin{align}\label{eq4c9502309fc844ff23795a8fdf756efa}
{g}^{\mu}_{} = {{A}^{\mu}_{}}   {{\nabla} ^ {-2}}   {\eta} = {{\nabla} ^ {-2}}   {\eta}   {\left({A}^{\mu}_{}\right)^T} = \left({{A}^{\mu}_{}}   {{\nabla} ^ {-2}}   {\eta}\right)^T = \left({g}^{\mu}_{}\right)^T = {\mathrm{symm}} . {\mathrm{const}},\end{align}
thus the \({g}^{\mu}_{}\) induce a Poisson bracket. \end{proof}

\begin{remark}\label{corollarya664c335c17a8b9d127c5ea17e2e8da0}

\begin{itemize}
\tightlist
\item
  Generally we can assume
  \({{{\nabla} ^ {2}} {\eta}} = {\mathrm{const}}\) if the system of
  first-order PDEs is linear.
\item
  If \({{\nabla} ^ {2}} {\eta}\) is singular, the \({A}^{\mu}_{}\) are
  \({2} \times {2}\) block-diagonal in coordinates in which
  \({{\nabla} ^ {2}} {\eta}\) is diagonal. The blocks associated with
  the non-zero eigenvalues of \({{\nabla} ^ {2}} {\eta}\) can be written
  using a constant hydrodynamic Poisson bracket, while we can say
  nothing about the other blocks.
\end{itemize}

\end{remark}

We note that, while all linear hyperbolic systems of conservation laws
with an additional conservation law admit a constant hydrodynamic
Poisson bracket, not all linear evolution equations induced by constant
hydrodynamic Poisson brackets are hyperbolic. Consider for example
\begin{align}\label{implies68f06fcf03560982046c61f766ee7e34}
{{g} = {\begin{bmatrix}1&0\\0&-1\end{bmatrix}}} , {{h\left({u}^{1}_{}, {u}^{2}_{}\right)} = {{{u}^{1}_{}}   {{u}^{2}_{}}}} \implies {A} = {\begin{bmatrix}0&1\\-1&0\end{bmatrix}}.\end{align}
Similarly, only in one spatial dimension is positive definiteness of the
matrices \({g}^{\mu}_{}\) sufficient for hyperbolicity. Consider for
example \begin{align}\label{implies5440344141a099376060cbd52c9c4be9}
{{{g}^{1}_{}} = {\begin{bmatrix}2&0\\0&1\end{bmatrix}}} , {{{g}^{2}_{}} = {\begin{bmatrix}1&0\\0&2\end{bmatrix}}} \implies {{g}^{1}_{}- {{g}^{2}_{}}} = {\begin{bmatrix}1&0\\0&-1\end{bmatrix}},\end{align}
i.e.~we may choose \(h\) as above such that in direction
\({\xi} = {\begin{bmatrix}1\\-1\end{bmatrix}}\) the system matrix
\(A\left(\xi\right)\) is not real diagonalizable.

In fact, we may show the following:
\begin{corollary}\label{corollary9a319b78c17b8a75883fc2a83f01a752} In
one spatial dimension (\(d=1\)) a constant hydrodynamic Poisson bracket
induces hyperbolic PDEs for all hydrodynamic functionals if and only if
its metric \(g\) is definite or identically zero. In multiple spatial
dimensions, \emph{in addition} all metrics \({g}^{\mu}_{}\) have to be
scalar multiples of each other.\end{corollary}

\begin{proof}\label{corollary9a319b78c17b8a75883fc2a83f01a752} For the
one-dimensional case, we may simply construct a Hamiltonian as above in
coordinates in which the metric is diagonal and contains only \(1, 0\)
or \(-1\). For the multi-dimensional case we note that from the 1D
result it follows that for every \emph{direction}
\({\xi} \in {{\mathcal{S}}^{\left[d-1\right]}_{}}\) the matrix
\(A\left(\xi\right)\) has to be either definite or identically zero.
From continuity of eigenvalues of \(A\left(\xi\right)\) (see
e.g.~\cite{Kato1995}, theorem 6.8) it follows that for all \(i, j\),
every curve connecting \({e}_{i}\) and \(\pm{{e}_{j}}\) on
\({\mathcal{S}}^{\left[d-1\right]}_{}\) has to pass a point \(\xi\)
where \({A\left(\xi\right)} = {0}\). This however can only happen, if
\(A\left({e}_{i}\right)\) is a scalar multiple of
\(A\left({e}_{j}\right)\). \end{proof}

Finally we want to note the following simple result, which will help us
relate non-constant hydrodynamic Poisson brackets to constant ones:
\begin{corollary}\label{corollary41b2fbcd4e0c18df5a164312b1ae81e2} Let
\(g\) be the constant metric associated with a constant hydrodynamic
Poisson bracket \(\pi\). After an invertible change of variables
\begin{align}\label{eq6f5a2f53a9618aadbb30041662ea2872}

        \hat{u}& = \hat{u}\left(u\right)
,\end{align} we have as transformed expressions
\begin{subequations}\label{relations31ea051525c89e3d7bf67df4f962aa48}
\begin{align}{\hat{g}}^{\mu, \hat{i}, \hat{j}}_{} &= {{\hat{u}}^{\hat{i}}_{i}}   {{g}^{\mu, i, j}_{}}   {{\hat{u}}^{\hat{j}}_{j}}\label{eqa43bc0434d62b052d9889217761b18c9}
,\\
{\hat{b}}^{\mu, \hat{i}, \hat{j}}_{\hat{k}} &= {{\hat{u}}^{\hat{i}}_{i}}   {{g}^{\mu, i, j}_{}}   {{\hat{u}}^{\hat{j}}_{j, k}}   {{u}^{k}_{\hat{k}}}\label{eq25989bbae94b94fe15191b86abdc6773}
\end{align} \end{subequations}

where
\begin{subequations}\label{relations3ed656bf0bc729ae25545db9002709d5}
\begin{align}{\hat{u}}^{\hat{i}}_{i} &= {\left[{\nabla}   {\hat{u}}\right]}^{\hat{i}, i}\label{eq6e38e364c38adf0ef1342a6dc5c71435}
,\\
{\hat{u}}^{\hat{j}}_{j, k} &= {\left[{{\nabla} ^ {2}}   {\hat{u}}\right]}^{\hat{j}, j, k}\label{eqb216065ebb8be352d08487976efd9549}
,\\
{u}^{k}_{\hat{k}} &= {\left[{\hat{\nabla}}   {u}\right]}^{k, \hat{k}}\label{eqd102e8e82738a786727df612f4429160}
\end{align} \end{subequations} \end{corollary}

\begin{proof}\label{corollary41b2fbcd4e0c18df5a164312b1ae81e2} A
straightforward computation yields
\begin{align}\label{eq901a76bd47136daa20df3e46a795a40c}

        \pi\left(F, H\right)& = \left<{F}_{i}, {{g}^{\mu, i, j}_{}}   {{\partial}^{}_{\mu}\left({H}_{j}\right)} + {\overbrace{{b}^{\mu, i, j}_{k}}^{0}}   {{u}^{k}_{\mu}}   {{H}_{j}}\right>\\
& = \left<{{\hat{F}}_{\hat{i}}}   {{\hat{u}}^{\hat{i}}_{i}}, {{g}^{\mu, i, j}_{}}   {{\partial}^{}_{\mu}\left({{\hat{u}}^{\hat{j}}_{j}}   {{\hat{H}}_{\hat{j}}}\right)}\right>\\
& = \left<{\hat{F}}_{\hat{i}}, {\underbrace{{{\hat{u}}^{\hat{i}}_{i}}   {{g}^{\mu, i, j}_{}}   {{\hat{u}}^{\hat{j}}_{j}}}_{{\hat{g}}^{\mu, \hat{i}, \hat{j}}_{}}}   {{\partial}^{}_{\mu}\left({\hat{H}}_{\hat{j}}\right)} + {\underbrace{{{\hat{u}}^{\hat{i}}_{i}}   {{g}^{\mu, i, j}_{}}   {{\hat{u}}^{\hat{j}}_{j, k}}   {{u}^{k}_{\hat{k}}}}_{{\hat{b}}^{\mu, \hat{i}, \hat{j}}_{\hat{k}}}}   {{\hat{u}}^{\hat{k}}_{\mu}}   {{\hat{H}}_{\hat{j}}}\right>\\
& = \hat{\pi}\left(\hat{F}, \hat{H}\right)
.\end{align}\end{proof}
