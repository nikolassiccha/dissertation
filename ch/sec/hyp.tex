% !TEX root = ../../main.tex
\hypertarget{hyperbolic-partial-differential-equations}{%
\section{Hyperbolic partial differential
equations}\label{hyperbolic-partial-differential-equations}}

Throughout, unless otherwise stated, we will use a \(d\)-dimensional
spatial manifold \(\mathcal{M}^d\), a simply connected \(n\)-dimensional
target manifold \(\mathcal{U}^n\) and assume
\({u\left(t\right)} \in {C^\infty\left(\mathcal{M}^d; \mathcal{U}^n\right)}\)
for all \(t\) considered. Furthermore, indices in products which appear
once above and once below will be summed. Greek indices like
\({\mu} , {\nu}\) run from \(1\) to \(d\) and latin indices like
\({i} , {j} , {k}\) run from \(1\) to \(n\), unless otherwise noted.

\begin{definition}[system of quasilinear first-order
PDEs]\label{definition261ec0b449061e6157ca265aa497244e} We consider
\emph{systems of quasilinear first-order partial differential equations}
\begin{align}\label{eqb94326c20f1cf9e1af95d1c59ad8525e}

        {{A}^{0, i}_{j}}   {{u}^{j}_{t}\left(x\right)} + {{A}^{\mu, i}_{j}\left(u\left(x\right)\right)}   {{u}^{j}_{\mu}\left(x\right)}& = {\sigma}^{i}_{}\left(u\left(x\right)\right)
.\end{align}\end{definition}

We are interested in the following subclasses:

\begin{definition}[hyperbolic]\label{definitiona593e85e2860d767213f4d194d2c7e5a}
A
\hyperref[definition261ec0b449061e6157ca265aa497244e]{system of quasilinear first-order PDEs}
is called \emph{hyperbolic} if the eigenvalue problem
\begin{align}\label{eq58dbc4eb553912626718ac4398788a2a}

        {\left[A\left(\xi\right)-{\lambda}   {{A}^{0}_{}}\right]}   {v}& = 0
\end{align} has real eigenvalues and \(n\) linearly independent
eigenvectors for every
\({\xi} \in {{\mathcal{S}}^{\left[d-1\right]}_{}}\), where
\begin{align}\label{comma07b3a03b3076ab4d461db5af4e9a0abf}

        A: {\mathcal{S}}^{\left[d-1\right]}_{} \rightarrow {T^{1}_{1}}   {\mathcal{U}^n}& , {A\left(\xi\right)} = {{{A}^{\mu}_{}}   {{\xi}_{\mu}}}
.\end{align} If \({{A}^{0}_{}} = {\mathrm{Id}}\) this translates to
\(A\left(\xi\right)\) being diagonalizable over the real numbers for
every direction
\({\xi} \in {{\mathcal{S}}^{\left[d-1\right]}_{}}\).\end{definition}

\textbf{Note:} Unless otherwise noted, we will assume
\({{A}^{0}_{}} = {\mathrm{Id}}\).

\begin{definition}[system of balance
laws]\label{definition88365c0138aa2205e81b796a2fa8d8a0} A
\hyperref[definition261ec0b449061e6157ca265aa497244e]{system of quasilinear first-order PDEs}
is called a system of \emph{balance laws} if there exist smooth flux
functions \({{F}^{\mu}_{}} \in {\Gamma\left(T\mathcal{U}^n\right)}\)
such that \begin{align}\label{eq4f562ad3e80019df1b24e424ed937f11}

        \frac{\partial {F}^{\mu, i}_{}}{\partial {u}^{j}_{}}& = {A}^{\mu, i}_{j}
\end{align} for \(\mu=0,\dots,d\). If \({\sigma} = {0}\) it is called a
system of \emph{conservation laws}.\end{definition}

\begin{definition}[symmetrizable]\label{definition5560523c9b90d17693db78f3deb39fb1}
A
\hyperref[definition261ec0b449061e6157ca265aa497244e]{system of quasilinear first-order PDEs}
is called \emph{symmetric hyperbolic} if \({A}^{0}_{}\) is symmetric
positive definite and all \({A}^{\mu}_{}\) are symmetric. A system that
can be brought into this form is called
\emph{symmetrizable}.\end{definition}

\begin{definition}[entropy-entropy flux pair
\(\left(\eta, q\right)\)]\label{definition0adeeb0d974a0a999d692b075a3ed64c}
A pair \(\left(\eta, q\right)\) is called an \emph{entropy-entropy flux
pair} to the
\hyperref[definition261ec0b449061e6157ca265aa497244e]{system of quasilinear first-order PDEs}
if the \emph{additional balance
law}\begin{align}\label{eqd702f32e5b391d49c859d85b4e791369}

        {\frac{\partial \eta}{\partial {u}^{i}_{}}}   {\left({{A}^{0, i}_{j}}   {{u}^{j}_{t}\left(x\right)} + {{A}^{\mu, i}_{j}\left(u\left(x\right)\right)}   {{u}^{j}_{\mu}\left(x\right)}\right)}& = {\eta}_{t}\left(u\left(x\right)\right) + {q}^{\mu}_{\mu}\left(u\left(x\right)\right)
\end{align} is implied by
(\ref{eqb94326c20f1cf9e1af95d1c59ad8525e}).\end{definition}

\begin{lemma}\label{lemma035dee2aae88c3e0a27b4ce34eaa877f} A
\hyperref[definition261ec0b449061e6157ca265aa497244e]{system of quasilinear first-order PDEs}
admits an
\hyperref[definition0adeeb0d974a0a999d692b075a3ed64c]{entropy-entropy flux pair $\left(\eta, q\right)$}
if the \emph{compatibility
condition}\begin{align}\label{eq71e11afa38fc842baa30fd2888e6086e}

        \frac{\partial {q}^{\mu}_{}}{\partial {u}^{j}_{}}& = {\frac{\partial \eta}{\partial {u}^{i}_{}}}   {{A}^{\mu, i}_{j}}
\end{align} is satisfied.\end{lemma}

\begin{lemma}\label{lemma198e9874082b60229f5dbb29a885f92a} Given an
entropy density \(\eta\), a
\hyperref[definition261ec0b449061e6157ca265aa497244e]{system of quasilinear first-order PDEs}
admits an
\hyperref[definition0adeeb0d974a0a999d692b075a3ed64c]{entropy-entropy flux pair $\left(\eta, q\right)$}
if and only if the \emph{integrability
condition}\begin{align}\label{eq84c0bedaad223970935fc47d0893d1fc}

        {\left[\frac{\partial}{\partial {u}^{k}_{}}\left({\frac{\partial \eta}{\partial {u}^{i}_{}}}   {{A}^{\mu, i}_{j}}\right)\right]}_{\left[j, k\right]}& = 0
\end{align} is satisfied.\end{lemma}

\begin{proof}Follows from lemma
\ref{lemma035dee2aae88c3e0a27b4ce34eaa877f} combined with the
requirement \begin{align}\label{eq8428826812f39cbabcd6a1d57d453add}
{\left[\frac{\partial^{2} {q}_{\mu}}{\partial {u}^{j}_{} \partial {u}^{k}_{}}\right]}_{\left[j, k\right]} = {\left[\frac{\partial}{\partial {u}^{k}_{}}\left({\frac{\partial \eta}{\partial {u}^{i}_{}}}   {{A}^{\mu, i}_{j}}\right)\right]}_{\left[j, k\right]} = 0\end{align}
and from the assertion that the \(n\)-dimensional target manifold
\(\mathcal{U}^n\) is simply connected (see e.g.~\cite{Zori2016}, section
14.3).\end{proof}

\begin{lemma}\label{lemma8f51ecac5eea41c78196495a7327fc42} A
\hyperref[definition88365c0138aa2205e81b796a2fa8d8a0]{system of balance laws}
admitting an
\hyperref[definition0adeeb0d974a0a999d692b075a3ed64c]{entropy-entropy flux pair $\left(\eta, q\right)$}
with strictly convex entropy density \(\eta\) is
\hyperref[definition5560523c9b90d17693db78f3deb39fb1]{symmetrizable}.\end{lemma}

\begin{proof}See e.g.~\cite{Frie1971}.\end{proof}

\begin{lemma}\label{lemma9d778dd404cfbdf295aebec846b6340f} A
\hyperref[definition261ec0b449061e6157ca265aa497244e]{system of quasilinear first-order PDEs}
can be written as a
\hyperref[definition88365c0138aa2205e81b796a2fa8d8a0]{system of balance laws}
in original coordinates \(u\) if
\begin{align}\label{eqa730f4cfa2b52e95f797a444569457ec}

        {\left[\frac{\partial {A}^{\mu, i}_{j}}{\partial {u}^{k}_{}}\right]}_{\left[j, k\right]}& = 0
\end{align} holds. \end{lemma}

\begin{proof}

Follows from the requirement
\begin{align}\label{eqfc06b32f722c96c1dbbe700cdc9f5075}
{\left[\frac{\partial^{2} {F}^{\mu, i}_{}}{\partial {u}^{j}_{} \partial {u}^{k}_{}}\right]}_{\left[j, k\right]} = {\left[\frac{\partial {A}^{\mu, i}_{j}}{\partial {u}^{k}_{}}\right]}_{\left[j, k\right]} = 0\end{align}
and from the assertion that the \(n\)-dimensional target manifold
\(\mathcal{U}^n\) is simply connected.\end{proof}

It has been shown that if the entropy and the fluxes are
\emph{homogeneous}, symmetrizability, admitting an entropy-entropy flux
pair and having a so called \emph{skew- selfadjoint form} are all
equivalent (see \cite{Tadm1984}, theorem 2.1 for details).

A weaker requirement than homogeneity and having a skew-selfadjoint form
is the following:
\begin{lemma}\label{lemma97924dafd18947a120b4f809fceaa333} Consider an
\emph{entropy functional}
\({H} = {\int \left(\eta\left(u\left(x\right)\right)\right)\, dx}\) and
a \emph{skew-symmetric bilinear
form}\begin{align}\label{eqc8657fe8ff1d7106061e0d1651c91e3c}

        L\left(v, w\right)& = \left<{v}_{i}, {\left({{g}^{\mu, i, j}_{}}   {\frac{\partial }{\partial {x}_{\mu}}} + {{b}^{\mu, i, j}_{k}}   {{u}^{k}_{\mu}}\right)}   {{w}_{j}}\right>
.\end{align} Then the \emph{evolution
equation}\begin{align}\label{eq48daed529c8ef98405d6b97214e5b487}

        {{u}^{i}_{t}} = {{\left[{L}   {\frac{\partial \eta}{\partial u}}\right]}^{i}}& = {\left({{g}^{\mu, i, j}_{}}   {{\eta}_{j, k}} + {{b}^{\mu, i, j}_{k}}   {{\eta}_{j}}\right)}   {{u}^{k}_{\mu}}
\end{align} admits an
\hyperref[definition0adeeb0d974a0a999d692b075a3ed64c]{entropy-entropy flux pair $\left(\eta, q\right)$}.
If \begin{align}\label{eq7e3424024ca21f6be40dcbc796186f27}

        {\left[\frac{\partial}{\partial {u}^{k}_{}}\left({{b}^{\mu, j, i}_{l}}   {{\eta}_{j}}\right)\right]}_{\left[k, l\right]}& = 0
,\end{align} it is a
\hyperref[definition88365c0138aa2205e81b796a2fa8d8a0]{system of balance laws}.
If the entropy density \(\eta\) is strictly convex, the system is
\hyperref[definition5560523c9b90d17693db78f3deb39fb1]{symmetrizable}.\end{lemma}

\begin{proof}First we note that from the skew-symmetry of \(L\) it
follows that \begin{align}\label{eqe82ccf41b61e376397e84f808f40e50d}

        {2}   {L\left(v, w\right)}& = {2}   {\left<{v}_{i}, {{b}^{\mu, i, j}_{k}}   {{u}^{k}_{\mu}}   {{w}_{j}} + {{g}^{\mu, i, j}_{}}   {{w}_{j, \mu}}\right>}\\
& = L\left(v, w\right)- {L\left(w, v\right)}\\
& = {2}   {{\left<{v}_{i}, {{b}^{\mu, i, j}_{k}}   {{u}^{k}_{\mu}}   {{w}_{j}}\right>}_{\left[v, w\right]}} + {\frac{1}{2}}   {\left(\left<{v}_{i}, {{g}^{\mu, i, j}_{}}   {{w}_{j, \mu}}\right>- {\left<{w}_{i}, {{g}^{\mu, i, j}_{}}   {{v}_{j, \mu}}\right>}\right)}\\
& = {2}   {\left<{v}_{i}, {{\left[{b}^{\mu, i, j}_{k}\right]}_{\left[i, j\right]}}   {{u}^{k}_{\mu}}   {{w}_{j}}\right>} + \left<{v}_{j}, \frac{\partial}{\partial {x}_{\mu}}\left({{g}^{\mu, i, j}_{}}   {{w}_{i}}\right)\right> + \left<{v}_{i}, {{g}^{\mu, i, j}_{}}   {{w}_{j, \mu}}\right>\\
& = {2}   {\left<{v}_{i}, {{\left[{b}^{\mu, i, j}_{k}\right]}_{\left[i, j\right]}}   {{u}^{k}_{\mu}}   {{w}_{j}}\right>} + \left<{v}_{j}, {\frac{\partial {g}^{\mu, i, j}_{}}{\partial {u}^{k}_{}}}   {{u}^{k}_{\mu}}   {{w}_{i}}\right> + {2}   {\left<{v}_{i}, {{\left[{g}^{\mu, i, j}_{}\right]}_{\left(i, j\right)}}   {{w}_{j, \mu}}\right>}\\
& = \left<{v}_{i}, {\underbrace{\left({2}   {{\left[{b}^{\mu, i, j}_{k}\right]}_{\left[i, j\right]}} + \frac{\partial {g}^{\mu, j, i}_{}}{\partial {u}^{k}_{}}\right)}_{{2}   {{b}^{\mu, i, j}_{k}}}}   {{u}^{k}_{\mu}}   {{w}_{j}} + {\underbrace{{2}   {{\left[{g}^{\mu, i, j}_{}\right]}_{\left(i, j\right)}}}_{{2}   {{g}^{\mu, i, j}_{}}}}   {{w}_{j, \mu}}\right>
.\end{align} Hence, we get
\begin{align}\label{ande98b94a2fb9a823688a6cdd380bf8ada}

        {{2}   {{\left[{b}^{\mu, i, j}_{k}\right]}_{\left(i, j\right)}}} = {\frac{\partial {g}^{\mu, i, j}_{}}{\partial {u}^{k}_{}}}& \text{ and } {{\left[{g}^{\mu, i, j}_{}\right]}_{\left[i, j\right]}} = {0}
.\end{align}

\textbf{To check the existence of an entropy-flux function:} From
(\ref{eq48daed529c8ef98405d6b97214e5b487}) we get
\begin{align}\label{eqb3ffe35df3acbf2cf272bc3a78e689c0}

        {\eta}_{t}\left(u\left(x\right)\right)& = {\frac{\partial \eta}{\partial {u}^{i}_{}}}   {\left({{g}^{\mu, i, j}_{}}   {{\eta}_{j, k}} + {{b}^{\mu, i, j}_{k}}   {{\eta}_{j}}\right)}   {{u}^{k}_{\mu}}
\end{align} and thus identify the \emph{gradient of the entropy-flux
function}\begin{align}\label{eqadeda78087718e6bfaaf485143a88c08}

        \frac{\partial {q}^{\mu}_{}}{\partial {u}^{k}_{}}& = {\frac{\partial \eta}{\partial {u}^{i}_{}}}   {\left({{b}^{\mu, i, j}_{k}}   {\frac{\partial \eta}{\partial {u}^{j}_{}}} + {{g}^{\mu, i, j}_{}}   {\frac{\partial^{2} \eta}{\partial {u}^{j}_{} \partial {u}^{k}_{}}}\right)}
\end{align} and can check the conditions of lemma
\ref{lemma198e9874082b60229f5dbb29a885f92a}:
\begin{align}\label{eqbe9078339183a14b99e3ad77d7dcb39b}

        {\left[\frac{\partial^{2} {q}^{\mu}_{}}{\partial {u}^{k}_{} \partial {u}^{l}_{}}\right]}_{\left[k, l\right]}& = {\left[\frac{\partial}{\partial {u}^{l}_{}}\left({\frac{\partial \eta}{\partial {u}^{i}_{}}}   {\left({{b}^{\mu, i, j}_{k}}   {\frac{\partial \eta}{\partial {u}^{j}_{}}} + {{g}^{\mu, i, j}_{}}   {\frac{\partial^{2} \eta}{\partial {u}^{j}_{} \partial {u}^{k}_{}}}\right)}\right)\right]}_{\left[k, l\right]}\\
& = {\left[\underbrace{{{\eta}_{i, l}}   {{b}^{\mu, i, j}_{k}}   {{\eta}_{j}}}_{{{\eta}_{i}}   {{b}^{\mu, j, i}_{k}}   {{\eta}_{j, l}}} + \underbrace{{{\eta}_{i, l}}   {{g}^{\mu, i, j}_{}}   {{\eta}_{j, k}}}_{{{\left(\right)}_{\left[k, l\right]}} = {0}} + \dots\right]}_{\left[k, l\right]}\\
& = {\left[\dots + {{\eta}_{i}}   {\left(\frac{\partial}{\partial {u}^{l}_{}}\left({{b}^{\mu, i, j}_{k}}   {{\eta}_{j}}\right) + \underbrace{\frac{\partial}{\partial {u}^{l}_{}}\left({{g}^{\mu, i, j}_{}}   {{\eta}_{j, k}}\right)}_{{{\left(\right)}_{\left[k, l\right]}} = {{\left[{{g}^{\mu, i, j}_{l}}   {{\eta}_{j, k}}\right]}_{\left[k, l\right]}}}\right)}\right]}_{\left[k, l\right]}\\
& = {\left[{{\eta}_{i}}   {\left({{b}^{\mu, j, i}_{k}}   {{\eta}_{j, l}} + \frac{\partial}{\partial {u}^{l}_{}}\left({{b}^{\mu, i, j}_{k}}   {{\eta}_{j}}\right) + {{g}^{\mu, i, j}_{l}}   {{\eta}_{j, k}}\right)}\right]}_{\left[k, l\right]}\\
& = {\left[{{\eta}_{i}}   {\left({\underbrace{{2}   {{\left[{b}^{\mu, j, i}_{k}\right]}_{\left(i, j\right)}}}_{{g}^{\mu, i, j}_{k}}}   {{\eta}_{j, l}} + {\frac{\partial {b}^{\mu, i, j}_{k}}{\partial {u}^{l}_{}}}   {{\eta}_{j}} + {{g}^{\mu, i, j}_{l}}   {{\eta}_{j, k}}\right)}\right]}_{\left[k, l\right]}\\
& = {\left[\underbrace{{{\eta}_{i}}   {\left({{g}^{\mu, i, j}_{k}}   {{\eta}_{j, l}} + {{g}^{\mu, i, j}_{l}}   {{\eta}_{j, k}}\right)}}_{{{\left(\right)}_{\left[k, l\right]}} = {0}} + \underbrace{{{\eta}_{i}}   {\frac{\partial {b}^{\mu, i, j}_{k}}{\partial {u}^{l}_{}}}   {{\eta}_{j}}}_{{} = {{{\eta}_{i}}   {{\left[\frac{\partial {b}^{\mu, i, j}_{k}}{\partial {u}^{l}_{}}\right]}_{\left(i, j\right)}}   {{\eta}_{j}}} = {{\frac{1}{2}}   {{\eta}_{i}}   {\frac{\partial {g}^{\mu, i, j}_{k}}{\partial {u}^{l}_{}}}   {{\eta}_{j}}}}\right]}_{\left[k, l\right]}\\
& = {\left[{\frac{1}{2}}   {{\eta}_{i}}   {\frac{\partial^{2} {g}^{\mu, i, j}_{}}{\partial {u}^{k}_{} \partial {u}^{l}_{}}}   {{\eta}_{j}}\right]}_{\left[k, l\right]}\\
& = 0
.\end{align}

\textbf{To check the existence of flux functions:} From the evolution
equation we identify
\({{A}^{\mu, i}_{k}} = {{{g}^{\mu, i, j}_{}} {{\eta}_{j, k}} + {{b}^{\mu, i, j}_{k}} {{\eta}_{j}}}\).
We check \begin{align}\label{eqb7d49c838e6eac0542aba04850c85e23}

        {\left[\frac{\partial {A}^{\mu, i}_{k}}{\partial {u}^{l}_{}}\right]}_{\left[k, l\right]}& = {\left[\frac{\partial}{\partial {u}^{l}_{}}\left({{g}^{\mu, i, j}_{}}   {{\eta}_{j, k}}\right) + \frac{\partial}{\partial {u}^{l}_{}}\left({{b}^{\mu, i, j}_{k}}   {{\eta}_{j}}\right)\right]}_{\left[k, l\right]}\\
& = {\left[\frac{\partial}{\partial {u}^{l}_{}}\left({{g}^{\mu, i, j}_{}}   {{\eta}_{j, k}}\right) + \frac{\partial}{\partial {u}^{l}_{}}\left({{b}^{\mu, i, j}_{k}}   {{\eta}_{j}}\right)\right]}_{\left[k, l\right]}\\
& = {\left[{\frac{\partial {g}^{\mu, i, j}_{}}{\partial {u}^{l}_{}}}   {{\eta}_{j, k}} + {{g}^{\mu, i, j}_{}}   {\frac{\partial {\eta}_{j, k}}{\partial {u}^{l}_{}}} + {\frac{\partial {b}^{\mu, i, j}_{k}}{\partial {u}^{l}_{}}}   {{\eta}_{j}} + {{b}^{\mu, i, j}_{k}}   {\frac{\partial {\eta}_{j}}{\partial {u}^{l}_{}}}\right]}_{\left[k, l\right]}\\
& = {\left[{{b}^{\mu, j, i}_{l}}   {{\eta}_{j, k}} + \underbrace{{{b}^{\mu, i, j}_{l}}   {{\eta}_{j, k}} + {{b}^{\mu, i, j}_{k}}   {{\eta}_{j, l}}}_{{{\left(\right)}_{\left[k, l\right]}} = {0}} + {{\eta}_{j}}   {\frac{\partial {b}^{\mu, i, j}_{k}}{\partial {u}^{l}_{}}}\right]}_{\left[k, l\right]}\\
& = {\left[\frac{\partial}{\partial {u}^{k}_{}}\left({{b}^{\mu, j, i}_{l}}   {{\eta}_{j}}\right)-{\frac{\partial {b}^{\mu, j, i}_{l}}{\partial {u}^{k}_{}}}   {{\eta}_{j}} + {{\eta}_{j}}   {\frac{\partial {b}^{\mu, i, j}_{k}}{\partial {u}^{l}_{}}}\right]}_{\left[k, l\right]}\\
& = {\left[\frac{\partial}{\partial {u}^{k}_{}}\left({{b}^{\mu, j, i}_{l}}   {{\eta}_{j}}\right) + {{\eta}_{j}}   {\frac{\partial^{2} {g}^{\mu, i, j}_{}}{\partial {u}^{k}_{} \partial {u}^{l}_{}}}\right]}_{\left[k, l\right]}\\
& = {\left[\frac{\partial}{\partial {u}^{k}_{}}\left({{b}^{\mu, j, i}_{l}}   {{\eta}_{j}}\right)\right]}_{\left[k, l\right]}
\end{align}

which completes the proof.\end{proof}
