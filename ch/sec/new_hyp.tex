% !TEX root = ../../main.tex
IMPORTANT INTRO

This chapter is structured as follows:

\begin{itemize}
\tightlist
\item
  First, in REFERENCE we give a concise introduction to hyperbolic
  systems of quasilinear first-order PDEs
  \ref{Symbol5a845d46a5cefcaae606da385b3fb9f4}.
\item
  Then, in REFERENCE we give a more in-depth discussion of the GENERIC
  framework.
\item
  In REFERENCE we discuss the crucial ingredient of GENERIC, Poisson
  structures.
\item
  In REFERENCE we discuss some well-known results about Poisson brackets
  of hydrodynamic type.
\item
  Finally, in REFERENCE we present our classification results of
  \emph{hyperbolic} Poisson brackets of hydrodynamic type.
\end{itemize}

Throughout this chapter we will stay on a \(d\)-dimensional smooth
spatial manifold \(\mathcal{M}^{d}\) for some fixed number of spatial
dimensions \(d \in \left(\mathbb{N}^{+}\right)\), where the spatial
manifold \(\mathcal{M}^{d}\) will typically be an appropriate torus
\(\mathcal{T}^{d}\). We will denote by
\(n\in\left(\mathbb{N}^{+}\right)\) the total number of unknown fields.
Our infinte-dimensional phase space \(\mathcal{U}\) will consist of
smooth mappings from the spatial manifold \(\mathcal{M}^{d}\) onto some
simply connected \(n\)-dimensional value manifold \(\mathcal{V}^{n}\),
i.e.~ \begin{align}
\label{Subsete7e05e51e61f739f8322c456dddeae99}\mathcal{U}& \subset  C^{\infty}\left(\left(\mathcal{M}^{d}\right) ; \left(\mathcal{V}^{n}\right)\right)
\end{align} which may have further structure, e.g.~due to our unknown
fields \(u \in \mathcal{U}\) consisting of vector fields. Indices in
products which appear once above and once below will be summed. Greek
indices like \(\mu, \nu\) run from \(1\) to \(d\) and latin indices like
\(i, j, k\) run from \(1\) to \(n\), unless otherwise noted. Partial
time or space derivatives will be denoted by a subscript \(t\) or
\(\mu\) where this introduces no ambiguity. Finally, for the sake of
brevity and legibility we will often suppress the explicit dependence of
local functions on the local values of the unknown fields \(u\). For
example, we will write \begin{align}
\label{Eq612442fb70e56a3a1f04482f9c3cf9f6}u_{t}&= \mathrm{div}\left(F\right)
\end{align} to symbolize \begin{align}
\label{Eq98b57f5c2f1ec756e645d7d7d99d2785}\partial_{t}\left(u\left(t, x\right)\right)&= \sum_{\mu} \partial_{\mu}\left(F^{\mu}\left(u\left(t, x\right)\right)\right)
.\end{align} This should not introduce any ambiguity, as we will always
simultaneously specify the domain and codomain of the appearing
functions. Furthermore, \emph{all} relevant functions will only depend
on local values of the unknown fields \(u\).

\hypertarget{hyperbolic-partial-differential-equations-of-hydrodynamic-type}{%
\section{Hyperbolic partial differential equations of hydrodynamic
type}\label{hyperbolic-partial-differential-equations-of-hydrodynamic-type}}

In the most general setting, we consider an arbitrary system of
quasilinear first-order PDEs \begin{align}
\label{Eq9ce772e7d0e7db463bb68ae86a67699f}A_{0} u_{t}+A_{\mu} u_{\mu}&= \sigma
\end{align} with appropriate initial conditions and boundary conditions
where applicable and with provided temporal coefficient matrix
\begin{align}
\label{Inf812d3f13fc26834ada22b1b1defbe5a}A_{0}& \in  C^{\infty}\left(\left(\mathcal{V}^{n}\right) ; \mathrm{Lin}\left(\mathcal{V}^{n}, \mathcal{V}^{n}\right)\right)
,\end{align} spatial coefficient matrices \begin{align}
\label{In68ae5ef73a28d75c399eab6cbee7e35b}A_{\mu}& \in  C^{\infty}\left(\left(\mathcal{V}^{n}\right) ; \mathrm{Lin}\left(\mathcal{V}^{n}, \mathcal{V}^{n}\right)\right)
\end{align} and source term \begin{align}
\label{In5afee3dc7b2c1ab4bba9b6b9874ddd74}\sigma& \in  C^{\infty}\left(\left(\mathcal{V}^{n}\right) ; \left(\mathcal{V}^{n}\right)\right)
.\end{align} For a system of quasilinear first-order PDEs
\ref{Eq9ce772e7d0e7db463bb68ae86a67699f} with associated projected
coefficient matrix \begin{align}
\label{Eqe6de0744dd37d2d5646fde4b68e40a14}A\left(\xi\right)&= A_{\mu} \xi_{\mu}
\end{align} to be \emph{hyperbolic}, the eigenvalue problem
\begin{align}
\label{Eq7a3812b280c0331eec53c9b9b2d1dd1b}\left(\left[A\left(\xi\right)-\lambda A_{0}\right]\right) v&= 0
\end{align} with eigenvalue \(\lambda \in \mathbb{C}\) and eigenvector
\(v\) has to admit only \emph{real} eigenvalues \(\lambda\) and has to
admit a set of linearly independent eigenvectors for every direction
\(\xi \in \left(\mathcal{S}^{d-1}\right)\).

\emph{However}, the structure of the GENERIC
\ref{Eq3d87841c0a6b988cff90b9173507ff07} favours the case that the
temporal coefficient matrix \(A_{0}\) is just the identity matrix,
although the more general case can usually be recovered by a simple
change of coordinates. We will thus always assume \(A_{0}=\mathrm{Id}\).
Then, the condition for hyperbolicity reduces to requiring that the
projected coefficient matrix \ref{Eqe6de0744dd37d2d5646fde4b68e40a14} is
diagonalizable over the real numbers for every direction
\(\xi \in \left(\mathcal{S}^{d-1}\right)\). A general system of
quasilinear first-order PDEs \ref{Eq9ce772e7d0e7db463bb68ae86a67699f}
being hyperbolic has two important consequences,

\begin{itemize}
\tightlist
\item
  FINITE PROPAGATION SPEED and
\item
  well-posed Cauchy problem (for smooth initial data) REFERENCE while
  failure to be hyperbolic indeed implies ill-posedness of the Cauchy
  problem REFERENCE.
\end{itemize}

This finite propagation speed enables the efficient solution of
hyperbolic systems of balance laws \begin{align}
\label{Eqa32cfbffd07c1e85e3b4d3cab9b5f452}u_{t}+\mathrm{div}\left(F\right)&= \sigma
,\end{align} which are defined via their flux functions \begin{align}
\label{Ina237af685877d83c7402a1e8d6ffbeb3}F& \in  C^{\infty}\left(\left(\mathcal{V}^{n}\right) ; \left(\underbrace{\left(\mathcal{V}^{n}\right) \times \dots \times \left(\mathcal{V}^{n}\right)}_{d}\right)\right)
,\end{align} using finite volume methods, giving rise to the famous
Courant-Friedrichs-Lewy condition \begin{align}
\label{Lefa88bd947ee3286ac5864254bfc4613d}\frac{\lambda \left(\Delta t\right)}{\Delta x}& \leq  C_{\mathrm{max}}
,\end{align} where in the simplest case the eigenvalue \(\lambda\)
represents the maximal propagation speed and the maximal Courant number
\(C_{\mathrm{max}}\) is one.

A general system of quasilinear first-order PDEs
\ref{Eq9ce772e7d0e7db463bb68ae86a67699f} with \(A_{0}=\mathrm{Id}\) can
be written as a system of balance laws
\ref{Eqa32cfbffd07c1e85e3b4d3cab9b5f452} if the flux functions
\ref{Ina237af685877d83c7402a1e8d6ffbeb3} exist, i.e.~if they relate to
the spatial coefficient matrices
\ref{In68ae5ef73a28d75c399eab6cbee7e35b} via the flux compatibility
condition \begin{align}
\label{Eq7d2e8241a2d4b2cafc766e0bbc2b24ff}\nabla F^{\mu}&= A_{\mu}
.\end{align} For the existence and uniqueness of weak solutions to
systems of balance laws \ref{Eqa32cfbffd07c1e85e3b4d3cab9b5f452} the
existence of a so called entropy-entropy flux pair
\(\left(\eta,q\right)\) is crucial. An entropy density \begin{align}
\label{In21e275a8b1c5e7370491070e1b539c57}\eta& \in  C^{\infty}\left(\left(\mathcal{V}^{n}\right) ; \mathbb{R}\right)
\end{align} and an entropy flux \begin{align}
\label{Ine93a565d83a1357f94af6361fcae1d66}q& \in  C^{\infty}\left(\left(\mathcal{V}^{n}\right) ; \left(\mathbb{R}^{d}\right)\right)
\end{align} form an entropy-entropy flux pair \(\left(\eta,q\right)\) of
the system of quasilinear first-order PDEs
\ref{Eq9ce772e7d0e7db463bb68ae86a67699f} with \(A_{0}=\mathrm{Id}\) and
\(\sigma=0\) if they satisfy the entropy compatibility condition
\begin{align}
\label{Eqca2b66a96ed57664b39a750b7c5556bd}\nabla q_{\mu}&= \left(\nabla \eta\right) A_{\mu}
.\end{align} Then, in the smooth regime, the entropy density \(\eta\) is
an additional conserved quantity and satisfies the entropy conservation
law \begin{align}
\label{Eqd1f947e5e59b66512cfe4900dddd9f1b}\eta_{t}+\mathrm{div}\left(q\right)&= 0
.\end{align} For appropriate source terms
\ref{In5afee3dc7b2c1ab4bba9b6b9874ddd74} we also have the entropy
balance law \begin{align}
\label{Eq2b5135f128b5dcf45f271a5388e9d2c3}\eta_{t}+\mathrm{div}\left(q\right)&= \left(\left\langle \nabla \eta, \sigma\right\rangle\right) \geq 0
,\end{align} i.e.~if the last inequality holds for all values
\(v \in \left(\mathcal{V}^{n}\right)\), the negative entropy is a
locally and globally non-decreasing quantity.

Another important idea is the \emph{symmetrizability} of systems of
quasilinear first-order PDEs \ref{Eq9ce772e7d0e7db463bb68ae86a67699f}. A
system of quasilinear first-order PDEs
\ref{Eq9ce772e7d0e7db463bb68ae86a67699f} is called \emph{symmetric} if
the temporal coefficient matrix \(A_{0}\) is symmetric positive definite
and the spatial coefficient matrices \(A_{\mu}\) are symmetric. A system
of quasilinear first-order PDEs \ref{Eq9ce772e7d0e7db463bb68ae86a67699f}
that can be brought into this form by a change of variables is called
\emph{symmetrizable}.

A celebrated result connects the above ideas:
\begin{lemma}\label{lemma0c5416161a2010aa1f1db191323f269a}
Godunov-Bou\ldots{}\end{lemma}

\begin{proof}DUMMY\end{proof}

Finally for symmetric systems of quasilinear first-order PDEs
\ref{Eq9ce772e7d0e7db463bb68ae86a67699f} the following is known:
\begin{lemma}\label{lemmac6d5c5db7b18ee4234ac0a5b733c4ba5}
well-posedness\end{lemma}

\begin{proof}DUMMY\end{proof}

\hypertarget{outlook---connection-to-generic}{%
\subsection{Outlook - connection to
GENERIC}\label{outlook---connection-to-generic}}

There exists an immediate connection of the above to GENERIC systems, or
rather already to reversible operators
\ref{Mapsa702fc956d9e98fde36cfc0718fe573c}. Without further conditions
but antisymmetry of the reversible operator \(\mathcal{L}\) we get the
existence of an entropy-entropy flux pair \(\left(\eta,q\right)\) for
systems of quasilinear first-order PDEs
\ref{Eq9ce772e7d0e7db463bb68ae86a67699f} that are induced by a
reversible operator \(\mathcal{L}\) and a \emph{hydrodynamic} total
energy \ref{Ind171fac6a03f77429d9940f431859de3}. First, we call any
functional \begin{align}
\label{In9f372ee398a9a82299011dc5ab2d94d7}\mathcal{F}& \in  C^{\infty}\left(\mathcal{U} ; \mathbb{R}\right)
\end{align} \emph{hydrodynamic} if it is of the form \begin{align}
\label{Eqc2a40c3c62eb22a5b1d9b9e417fa3d3e}\mathcal{F}\left(u\right)&= \int_{X} f\left(u\left(x\right)\right) dx
\end{align} for some smooth functional density \begin{align}
\label{In8074060ebe24d6326ae062357649de76}f& \in  C^{\infty}\left(\left(\mathcal{V}^{n}\right) ; \mathbb{R}\right)
.\end{align} The gradient of such a functional \(\mathcal{F}\) can then
be identified with the gradient of its functional density \(f\), i.e.~
\begin{align}
\label{Eqc8d3f53f9fffd947c79200104aa2f931}\nabla \mathcal{F}&= \nabla f
.\end{align} A reversible operator \(\mathcal{L}\) is called
\emph{hydrodynamic} if, given a hydrodynamic total energy
\(\mathcal{H}\), it produces a system of quasilinear first-order PDEs
\ref{Eq9ce772e7d0e7db463bb68ae86a67699f} via \begin{align}
\label{Eqc106a0a2a35f60f0e7ab300f515d1d3d}u_{t}&= \mathcal{L} \left(\nabla\mathcal{H}\right)
.\end{align} For a reversible operator \(\mathcal{L}\) to be
hydrodynamic, it has to act on ``vectors'' \(v\) as \begin{align}
\label{Eqfa8a5782f80e23a2d963d7c87ea58ab6}\left(\mathcal{L} v\right)_{i}&= g_{\mu,i,j} v_{\mu,j}+b_{\mu,i,j,k} u_{\mu,k} v_{j}
\end{align} via some family of metrics \begin{align}
\label{In60b61b8ec681542f234e0e8999d39f42}g& \in  C^{\infty}\left(\left(\mathcal{V}^{n}\right) ; \left(\underbrace{\mathrm{Lin}\left(\mathcal{V}^{n}, \mathcal{V}^{n}\right) \times \dots \times \mathrm{Lin}\left(\mathcal{V}^{n}, \mathcal{V}^{n}\right)}_{d}\right)\right)
\end{align} and some family of ``monsters'' \begin{align}
\label{In4b5c9702fd5df4c0dcfae4fde12e8df3}b& \in  C^{\infty}\left(\left(\mathcal{V}^{n}\right) ; \left(\underbrace{\mathrm{Lin}\left(\mathcal{V}^{n}, \mathcal{V}^{n}, \mathcal{V}^{n}\right) \times \dots \times \mathrm{Lin}\left(\mathcal{V}^{n}, \mathcal{V}^{n}, \mathcal{V}^{n}\right)}_{d}\right)\right)
.\end{align} If we \emph{only} require antisymmetry of the reversible
operator \(\mathcal{L}\), then the family of metrics \(g\) has to
consist of symmetric pseudometrics \(g_{\mu}\) and compatible
``monsters'' \(b_{\mu}\). The antisymmetric compatibility between a
given symmetric pseudometric \(g_{\mu}\) and its ``monster'' \(b_{\mu}\)
is just \begin{align}
\label{Eqe52bb7ebbaa2a03b5248517bd21abb69}\mathrm{compatibility}&= \mathrm{DUMMY}
.\end{align}

It follows then at once that a system of quasilinear first-order PDEs
\ref{Eq9ce772e7d0e7db463bb68ae86a67699f} that is induced by a
hydrodynamic reversible operator \(\mathcal{L}\) and a hydrodynamic
total energy \(\mathcal{H}\) admits an additional local conservation law
for the total energy density \(h\):

\begin{lemma}\label{lemmab2f9855d0444e8ac22813d1fd363896a} energy
claw\end{lemma}

\begin{proof}DUMMY\end{proof}

The above result thus suggests to focus on strictly convex total energys
\(\mathcal{H}\) and reversible operators \(\mathcal{L}\) that induce not
only a system of quasilinear first-order PDEs
\ref{Eq9ce772e7d0e7db463bb68ae86a67699f} but a system of balance laws
\ref{Eqa32cfbffd07c1e85e3b4d3cab9b5f452}, as these then guarantee the
satisfaction of the preconditions of REFERENCE, immediately yielding
hyperbolicity and even symmetrizability. This is indeed one fruitful
avenue of attack that we will take and the following result is
immediate:

\begin{lemma}\label{lemma1a6ebda94f904644c076ed2a85fc371a} GENERIC
blaw\end{lemma}

\begin{proof}DUMMY\end{proof}

\emph{However}, before we continue, we will have to talk about
\emph{GENERIC systems of hydrodynamic type} more generally.

\hypertarget{generic-systems-of-hydrodynamic-type}{%
\section{GENERIC systems of hydrodynamic
type}\label{generic-systems-of-hydrodynamic-type}}
