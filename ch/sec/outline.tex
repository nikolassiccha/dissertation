% !TEX root = ../../main.tex
Both GENERIC systems (\emph{General Equation for Non-Equilibrium
Reversible- Irreversible Coupling}, \cite{Morr1984,Grme1997,Otti1997})
and hyperbolic balance laws (see e.g.~\cite{Benz2006}) have proven to be
ubiquitous in thermodynamics, with hyperbolic balance laws playing a
major role within the context of RET (\emph{Rational extended
thermodynamics}, \cite{Mull1998}) and SHTC equations (\emph{Symmetric
Hyperbolic Thermodynamically Compatible} equations,
\cite{Godu1996,Pave2020}).

It is well-known that in the smooth regime many hyperbolic and
hydrodynamic PDEs (quasilinear systems of first order partial
differential equations, \cite{Dubr1983a}) are time reversible with a
well-posed forward and backward Cauchy problem \cite{Benz2006}, while as
soon as discontinuities develop, time-reversibility is usually lost. In
the absence of regularizing higher-order contributions, discontinuities
generally form after finite time for nonlinear hydrodynamic PDEs.
Similarly, it is well-known that in the smooth regime many hydrodynamic
PDEs can be regarded as infinite-dimensional Hamiltonian systems, while
this property is generally lost in the presence of discontinuities.
These systems include among others the equations of ideal
magnetohydrodynamics \cite{Morr1980b} and their subsystems such as the
Euler equations or the related shallow water equations. For systems of
this kind, the total physical energy of the system usually acts as the
Hamiltonian with the entropy density being just another conserved
quantity without special significance.

The above facts immediately suggest to identify the reversible part of a
potential GENERIC system with its hydrodynamic part and to investigate
the connection between hyperbolicity and the existence of an underlying
infinite-dimensional Hamiltonian structure. As a first step we show that
except for the case of a single dependent variable together with a
severely restricted class of Hamiltonian functionals, the irreversible
part of a potential GENERIC system may not contribute to the
hydrodynamic part of its evolution equation. This then enables us to
focus exclusively on the Poisson part of GENERIC systems to answer the
question of their potential hyperbolicity.

We may thus build on extensive work on hydrodynamic Poisson systems
(\cite{Novi1982,Dubr1983a,Mokh1989,Bogo2007b}). Exploiting various
classification results for hydrodynamic Poisson brackets with
non-degenerate or degenerate metric(s), for low numbers of spatial
dimensions \(d \leq 3\) and low numbers of dependent variables
\(n \leq 4\) due to among others but most recently and most explicitly
Ferapontov, Lorenzoni and Savoldi (see
\cite{Fera2015,Savo2015,Savo2016b}), we investigate the question of
hyperbolicity of GENERIC systems. With the form of the Poisson bracket
fixed as one of the canonical forms due to the classification results,
the remaining degree of freedom capable of influencing the question of
hyperbolicity is the Hamiltonian functional. The conditions placed on
this Hamiltonian for hyperbolicity range from non-existent for
one-dimensional (\(n=1\) or \(d=1\)) Poisson systems with underlying
definite metric, over convexity as a sufficient condition for constant
Poisson brackets or a certain class of linear Poisson brackets, up to
triviality of the Hamiltonian for large classes of Poisson brackets with
underlying degenerate metrics.

\hypertarget{outline}{%
\section{Outline}\label{outline}}

This thesis consists of two main parts. One concerns itself with
\emph{structure-preserving} numerical integrators and their relevance to
GENERIC systems. The other discusses the connection between GENERIC
systems and hyperbolic PDEs, culminating in low-dimensional
classification results. While intimate knowledge of GENERIC systems and
in particular of the Poisson systems is needed for the second part of
this thesis, only a formal understanding is necessary for the part on
structure-preserving integrators. Thus, this thesis is organized as
follows:

\begin{itemize}
\tightlist
\item
  In \autoref{generic-digest} we will give a very concise introduction
  to the GENERIC framework to provide the reader with a backdrop for the
  chapter on numerical integrators. We will \emph{formally} introduce
  the central concepts that are needed to reason about these numerical
  integrators in the context of the GENERIC framework. A more in-depth
  discussion of the GENERIC framework will then follow at the beginning
  of the chapter on the connection between GENERIC systems and
  hyperbolic PDEs.
\item
  In \autoref{chap:dg} we present a novel family of timestepping
  methods, aimed at capturing a central property of purely reversible,
  purely irreversible and GENERIC systems, the \emph{free energy
  dissipation rate}. For this we will first provide a short overview of
  the literature concerning structure-preserving integrators and then go
  on to introduce our new family of timestepping methods. We will
  showcase the suitability of this family of timestepping methods for
  purely reversible, purely irreversible and mixed, linear and
  non-linear, ordinary and partial differential equations.
\item
  In \autoref{chap:hyp} we will then give a more in-depth introduction
  to the GENERIC framework and to hyperbolic PDEs. It will turn out that
  we will have to focus on the Poisson part of GENERIC systems, and we
  will thus present the relevant results from the extensive literature.
  Finally, we will provide a low-dimensional classification of
  hyperbolic Poisson systems, and with it a classification of the
  corresponding hyperbolic GENERIC systems.
\item
  In \autoref{chap:conclusion} we will then quickly summarize the main
  results and provide an outlook on possible future research directions.
\end{itemize}
