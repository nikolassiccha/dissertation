% !TEX root = ../../main.tex
In \cite{Pave2020}, the following result is stated (Theorem 2.10):

\begin{quote}
\begin{theorem}

Consider a one-dimensional Hamiltonian system of hydrodynamic type with
non-degenerate metric. Assuming that the energy of the system is of
hydrodynamic type, convex and a proper scalar, it follows that the
evolution equations can be regarded as a first-order quasilinear
symmetric hyperbolic PDE system. \end{theorem}
\end{quote}

This appears to be not correct. Consider the following diagonal metric
\(g\) with \emph{matrix
representation}\begin{align}\label{eq1b48ffb7ca707f5c40b21b2883d4307a}

        g\left(u\right)& = \left[\begin{matrix}{g}^{1}_{}\left({u}^{1}_{}\right) & 0\\0 & {g}^{2}_{}\left({u}^{2}_{}\right)\end{matrix}\right]
.\end{align} We get the corresponding \emph{differential
operator}\begin{align}\label{eq398c1f2df8aed822edd4589d646fa259}

        {L}   {v}& = {g\left(u\right)}   {\frac{\partial v}{\partial x}} + {\frac{1}{2}}   {\frac{\partial}{\partial {u}^{k}_{}}\left(g\left(u\right)\right)}   {{u}^{k}_{x}}   {v}\\
& = {\left[\begin{matrix}{g}^{1}_{} & 0\\0 & {g}^{2}_{}\end{matrix}\right]}   {\left[\begin{matrix}\frac{\partial {v}_{1}}{\partial {u}^{1}_{}} & \frac{\partial {v}_{1}}{\partial {u}^{2}_{}}\\\frac{\partial {v}_{2}}{\partial {u}^{1}_{}} & \frac{\partial {v}_{2}}{\partial {u}^{2}_{}}\end{matrix}\right]}   {\left[\begin{matrix}{u}^{1}_{x}\\{u}^{2}_{x}\end{matrix}\right]} + {\frac{1}{2}}   {\left[\begin{matrix}{\frac{\partial {g}^{1}_{}}{\partial {u}^{1}_{}}}   {{u}^{1}_{x}} & 0\\0 & {\frac{\partial {g}^{2}_{}}{\partial {u}^{2}_{}}}   {{u}^{2}_{x}}\end{matrix}\right]}   {\left[\begin{matrix}{v}_{1}\\{v}_{2}\end{matrix}\right]}\\
& = \left[\begin{matrix}{{g}^{1}_{}}   {\frac{\partial {v}_{1}}{\partial {u}^{1}_{}}}   {{u}^{1}_{x}} + {{g}^{1}_{}}   {\frac{\partial {v}_{1}}{\partial {u}^{2}_{}}}   {{u}^{2}_{x}} + {\frac{1}{2}}   {\frac{\partial {g}^{1}_{}}{\partial {u}^{1}_{}}}   {{u}^{1}_{x}}   {{v}_{1}}\\{{g}^{2}_{}}   {\frac{\partial {v}_{2}}{\partial {u}^{1}_{}}}   {{u}^{1}_{x}} + {{g}^{2}_{}}   {\frac{\partial {v}_{2}}{\partial {u}^{2}_{}}}   {{u}^{2}_{x}} + {\frac{1}{2}}   {\frac{\partial {g}^{2}_{}}{\partial {u}^{2}_{}}}   {{u}^{2}_{x}}   {{v}_{2}}\end{matrix}\right]\\
& = {\left[\begin{matrix}{{g}^{1}_{}}   {\frac{\partial {v}_{1}}{\partial {u}^{1}_{}}} + {\frac{1}{2}}   {\frac{\partial {g}^{1}_{}}{\partial {u}^{1}_{}}}   {{v}_{1}} & {{g}^{1}_{}}   {\frac{\partial {v}_{1}}{\partial {u}^{2}_{}}}\\{{g}^{2}_{}}   {\frac{\partial {v}_{2}}{\partial {u}^{1}_{}}} & {{g}^{2}_{}}   {\frac{\partial {v}_{2}}{\partial {u}^{2}_{}}} + {\frac{1}{2}}   {\frac{\partial {g}^{2}_{}}{\partial {u}^{2}_{}}}   {{v}_{2}}\end{matrix}\right]}   {\left[\begin{matrix}{u}^{1}_{x}\\{u}^{2}_{x}\end{matrix}\right]}
,\end{align} defined by its action on a generic field
\({{v} = {{\hat{v}} \circ {u}}} \in {C^\infty\left(\mathbb{R}; {\mathbb{R}} ^ {2}\right)}\).
The induced bracket is Poisson, as can be verified by inserting
\begin{subequations}\label{relations9fcad15ffdd804c0e4dacb758addc7d3}
\begin{align}{g}^{i, j}_{} &= {{\delta}^{i, j}_{}}   {{g}^{i}_{}}\label{eq64304aa7b04fdddcc6eafb9545e2b09c}
,\\
{b}^{i, j}_{k} &= {{\frac{1}{2}}   {\frac{\partial {g}^{i, j}_{}}{\partial {u}^{k}_{}}}} = {{\frac{1}{2}}   {{\delta}^{i, j}_{k}}   {\frac{\partial {g}^{i}_{}}{\partial {u}^{k}_{}}}}\label{eq5d64674e88fb9d4b5483815a6c60a0f9}
\end{align} \end{subequations} into
(\ref{relationsc8f5eee804ce8db284850ef06a183d08}). Here
\({\delta}^{i, j}_{k}\) is a generalization of the Kronecker delta,
which is \(1\) exactly if all indices are equal and \(0\) otherwise.
Inner products of \(g\) and \(b\) retain this structure, hence are
symmetric in all free indices, and thus all terms in the Jacobi identity
vanish.

Recall that (global) hyperbolicity means that the matrix in front of the
spatial gradient of the unknown fields is diagonalizable over the real
numbers (everywhere). A necessary condition for a \emph{real-valued
matrix}\begin{align}\label{eq609abc8726fd9c540b56ad96ce4aa4d3}

        A& = \left[\begin{matrix}a & b\\c & d\end{matrix}\right]
\end{align} to be real diagonalizable is for the discriminant of its
characteristic polynomial to be non-negative:
\begin{align}\label{geq33f10b1a36c44b079a228506dc8b659f}

        {\left(a- {d}\right)} ^ {2} + {4}   {b}   {c}& \geq 0
.\end{align} A sufficient condition for the above real-valued matrix
\(A\) to be symmetric positive definite is \[b = c, |b| < \min(a, d).\]
Let us now apply the above differential operator to the field associated
with the variational derivative of some (strictly) convex hydrodynamic
functional
\({H\left(u\right)} = {\int \left(h\left(u\left(x\right)\right)\right)\, dx}\).
We get \begin{align}\label{eq2bf36e569484a31912403e63647737f0}

        {{L}^{i, j}_{}}   {\frac{\partial h}{\partial {u}^{j}_{}}}& = \frac{\partial h}{\partial {u}^{a}_{}}\left({v}_{a}\left({\left[\begin{matrix}{{g}^{1}_{}}   {\frac{\partial {v}_{1}}{\partial {u}^{1}_{}}} + {\frac{1}{2}}   {\frac{\partial {g}^{1}_{}}{\partial {u}^{1}_{}}}   {{v}_{1}} & {{g}^{1}_{}}   {\frac{\partial {v}_{1}}{\partial {u}^{2}_{}}}\\{{g}^{2}_{}}   {\frac{\partial {v}_{2}}{\partial {u}^{1}_{}}} & {{g}^{2}_{}}   {\frac{\partial {v}_{2}}{\partial {u}^{2}_{}}} + {\frac{1}{2}}   {\frac{\partial {g}^{2}_{}}{\partial {u}^{2}_{}}}   {{v}_{2}}\end{matrix}\right]}   {\left[\begin{matrix}{u}^{1}_{x}\\{u}^{2}_{x}\end{matrix}\right]}\right)\right)\\
& = {A}   {\left[\begin{matrix}{u}^{1}_{x}\\{u}^{2}_{x}\end{matrix}\right]}
\end{align} and
\({\nabla_{i} \nabla_{j}h} = {{\left[{{g}^{-1}} {A}\right]}_{i, j}}\)
where \(\nabla_{i}\) represents the covariant derivative associated with
the Levi-Civita connection of the metric \(g\).

We observe:

\begin{itemize}
\tightlist
\item
  the metric \(g\) is flat and the above differential operator induces a
  Poisson bracket
\item
  the metric \(g\) is non-degenerate (everywhere), if
  \(g^1 \neq 0 \neq g^2\) (everywhere)
\item
  the energy functional \(H\) is of hydrodynamic type
\item
  the energy functional/density is (strictly) convex if
  \({\left| \frac{\partial^{2} h}{\partial {u}^{1}_{} \partial {u}^{2}_{}} \right|} < {\text{min}(\frac{\partial^{2} h}{\partial {u}^{1}_{} \partial {u}^{1}_{}}, \frac{\partial^{2} h}{\partial {u}^{2}_{} \partial {u}^{2}_{}})}\)
\item
  the energy density \(h\) is a proper scalar, if \(u^1\) and \(u^2\)
  are proper scalars.
\end{itemize}

However

\begin{itemize}
\tightlist
\item
  the signs \(\sigma\left({g}^{1}_{}\right)\) and
  \(\sigma\left({g}^{2}_{}\right)\) do not matter for non-degeneracy
\item
  the first order partial derivatives of the energy density
  \(\frac{\partial h}{\partial {u}^{1}_{}}\) and
  \(\frac{\partial h}{\partial {u}^{2}_{}}\) do not matter for convexity
\item
  the matrix defined by the entries \(\nabla_{i} \nabla_{j}h\), while
  symmetric, is \textbf{not} necessarily positive definite
\end{itemize}

and we can thus choose our non-degenerate metric \(g\) and construct a
(strictly) convex energy density \(h\) such that for some point \(u^*\)

\begin{itemize}
\tightlist
\item
  \({{g}^{1}_{}} < {0} < {{g}^{2}_{}}\)
\item
  \({0} < {\left| \frac{\partial^{2} h}{\partial {u}^{1}_{} \partial {u}^{2}_{}} \right|} < {\text{min}(\frac{\partial^{2} h}{\partial {u}^{1}_{} \partial {u}^{1}_{}}, \frac{\partial^{2} h}{\partial {u}^{2}_{} \partial {u}^{2}_{}})}\)
\item
  \({{\frac{1}{2}} {\frac{\partial {g}^{i}_{}}{\partial {u}^{i}_{}}} {\frac{\partial h}{\partial {u}^{i}_{}}}} = {-{{g}^{i}_{}} {\frac{\partial^{2} h}{\partial {u}^{i}_{} \partial {u}^{i}_{}}}}\)
  for \(i=1,2\).
\end{itemize}

The first two points ensure that the product of the off-diagonal entries
is (strictly) negative, whilst the last point ensures that the diagonal
entries vanish, hence yielding non-hyperbolicity at \(u^*\) and in a
neighbourhood around \(u^*\).

See also figure \ref{fig:discr} for a slightly different example.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{discr.pdf}
    \caption["Excess hyperbolicity" for a counterexample to \cite{Pave2020}, Theorem 2.10]{"Excess hyperbolicity" (${\left(a- {d}\right)} ^ {2} + {4}   {b}   {c}$) for ${{g}^{i}_{}} = {{\left(-1\right)^{i}}   {\left(1 + {{u}^{i}_{}} ^ {2}\right)}}$ and ${h\left(u\right)} = {\left<u, {\frac{1}{2}}   {\left[\begin{matrix}1 & 1- {\varepsilon}\\1- {\varepsilon} & 1\end{matrix}\right]}   {u} + \left[\begin{matrix}1\\0\end{matrix}\right]\right>}$, ${\varepsilon} = {0.1}$. White corresponds to non-hyperbolic regions.}
    \label{fig:discr}
\end{figure}

We may wonder whether there is something peculiar about the above
constructions. It is obvious that the above construction would not have
worked, if the metric were definite or if
\({\frac{\partial^{2} h}{\partial {u}^{1}_{} \partial {u}^{2}_{}}} = {0}\).
Are there special Poisson brackets or Hamiltonians which guarantee
hyperbolicity? The following Lemma gives the answer:

\begin{lemma}

Consider a Poisson bracket of hydrodynamic type with non-degenerate
metric \(g\) in one space dimension and with arbitrary number of fields
\(n \geq 2\).

\begin{itemize}
\tightlist
\item
  If the metric is \emph{definite}, \emph{every} Hamiltonian of
  hydrodynamic type induces a system of hyperbolic PDEs.
\item
  On the other hand, for \emph{every} (non-constant) Hamiltonian of
  hydrodynamic type, we can construct a Poisson bracket with
  non-degenerate metric such that the induced system of PDEs is
  non-hyperbolic on a set of non-zero measure.
\end{itemize}

\end{lemma}
\begin{proof}

We know that the metric \(g\) is flat. If it is definite, for every
point \(p \in \mathcal{U}^n\) we may find local coordinates \(\hat{u}\)
such that \begin{align}\label{eqfda1911a98a46ba27b4781e5a98c9380}

        {g}^{}_{i, j}& = {} \pm {{\delta}^{}_{i, j}}
\end{align} on a neighborhood of \(p\). On this neighborhood \(g\) is
constant, hence the Christoffel symbols of the second kind
(cf.~(\ref{eq07c0123be36e3c9ad83240634f73c557})) vanish and thus
\({b}^{i, j}_{k}\) also does.

In these coordinates, the evolution equation reads
\begin{align}\label{eqeb30989bfbcd6faf564bfc6828cc6eec}

        {{\hat{u}}^{}_{t}} = {{\hat{L}}   {\hat{\nabla}}   {h}}& = {g}   {{\hat{\nabla}} ^ {2}}   {\hat{h}}   {{\hat{u}}^{}_{x}}\\
& = {} \pm {{{\hat{\nabla}} ^ {2}}   {\hat{h}}   {{\hat{u}}^{}_{x}}}
.\end{align} Due to symmetry of the Hessian, the system is hyperbolic.
In fact, if we could ensure that the Hessian of the energy density were
positive definite in those flat coordinates \(\hat{u}\), we would also
immediately get hyperbolicity for indefinite metrics. However, given a
Hamiltonian of hydrodynamic type, we can in general guarantee neither
this sufficient condition nor a weaker one.

Let us first look at the \(2 \times 2\) case. Let
\begin{align}\label{eqd93f4102b7e59aedfd535d240afc7b63}

        {\hat{h}}_{\hat{i}, \hat{j}}& = {\left[{{\hat{\nabla}} ^ {2}}   {\hat{h}}\right]}_{\hat{i}, \hat{j}}
\end{align} denote the entries of the Hessian in flat coordinates
\(\hat{u}\) where the metric is constantly
\begin{align}\label{eq0d26dc9431c231365f180580f54c7c25}

        \hat{g}& = \begin{bmatrix}1&0\\0&-1\end{bmatrix}
.\end{align} Then
\begin{align}\label{eq1dcb5b9d604c28f7a1b0d6a086f3740a}

        A& = {\hat{g}}   {{\hat{\nabla}} ^ {2}}   {\hat{h}}
\end{align} is real diagonalizable if and only if
\begin{align}\label{geq4a083e29b9e69ced8cb17a5b2c72e08b}

        \left| {\hat{h}}_{1, 1} + {\hat{h}}_{2, 2} \right|& \geq {2}   {\left| {\hat{h}}_{1, 2} \right|}
,\end{align} which is weaker than convexity of \(h\).

The question now is, given some energy density \(h\), which is possibly
(strictly) convex in non-flat, ``original'' coordinates \(u\), can we
somehow guarantee hyperbolicity based on properties of the energy
density alone, independently of the Poisson bracket? The answer, as
stated in the lemma, is no:

Consider coordinates
\begin{align}\label{eq64327056607437e2d02c684d5552cd71}

        \begin{bmatrix}{u}^{1}_{}\\{u}^{2}_{}\end{bmatrix}& = \begin{bmatrix}{\hat{u}}^{1}_{}\\f\left({\hat{u}}^{1}_{}, {\hat{u}}^{2}_{}\right)\end{bmatrix}
\end{align} for some smooth real-valued function \(f\) with
\(f' \neq 0\). Denoting as entries of the Jacobian
\({{u}^{i}_{\hat{i}}} = {{{\hat{\nabla}}_{\hat{i}}} {{u}^{i}_{}}}\) we
get \begin{align}\label{eq8241edf295d91fa79896f90b31acc2b0}

        {\hat{h}}_{\hat{i}, \hat{j}}& = {{h}_{i, j}}   {{u}^{i}_{\hat{i}}}   {{u}^{j}_{\hat{j}}} + {{h}_{i}}   {{u}^{i}_{\hat{i}, \hat{j}}}\\
& = \begin{bmatrix}{{h}_{i, j}}   {{u}^{i}_{1}}   {{u}^{j}_{1}} + {{h}_{i}}   {{u}^{i}_{1, 1}}&{{h}_{i, j}}   {{u}^{i}_{1}}   {{u}^{j}_{2}} + {{h}_{i}}   {{u}^{i}_{1, 2}}\\{{h}_{i, j}}   {{u}^{i}_{2}}   {{u}^{j}_{1}} + {{h}_{i}}   {{u}^{i}_{2, 1}}&{{h}_{i, j}}   {{u}^{i}_{2}}   {{u}^{j}_{2}} + {{h}_{i}}   {{u}^{i}_{2, 2}}\end{bmatrix}\\
& = \begin{bmatrix}\left({{f}_{1}} ^ {2}\right) {h}_{2, 2} + {h}_{2} {f}_{1, 1} + 2 {f}_{1} {h}_{1, 2} + {h}_{1, 1}&{h}_{2} {f}_{1, 2} + {f}_{1} {f}_{2} {h}_{2, 2} + {f}_{2} {h}_{1, 2}\\{h}_{2} {f}_{2, 1} + {f}_{1} {f}_{2} {h}_{2, 2} + {f}_{2} {h}_{1, 2}&\left({{f}_{2}} ^ {2}\right) {h}_{2, 2} + {h}_{2} {f}_{2, 2}\end{bmatrix}
.\end{align} We get
\begin{align}\label{geqd4ca95817924e44ca618cbc495979262}

        \left| \left({{f}_{1}} ^ {2}\right) {h}_{2, 2} + \left({{f}_{2}} ^ {2}\right) {h}_{2, 2} + {h}_{2} {f}_{1, 1} + {h}_{2} {f}_{2, 2} + 2 {f}_{1} {h}_{1, 2} + {h}_{1, 1} \right|& \geq {2}   {\left| {h}_{2} {f}_{1, 2} + {f}_{1} {f}_{2} {h}_{2, 2} + {f}_{2} {h}_{1, 2} \right|}
\end{align} as a necessary condition for hyperbolicity. Without loss of
generality we will assume \(h_2(0) \neq 0\). Note that given some
function \(f\), we may construct a function
\({\hat{f}\left(x\right)} = {\frac{f\left({c} {x}\right)}{c}}\) such
that \({{\hat{f}}_{i}\left(0\right)} = {{f}_{i}\left(0\right)}\) but
\({{\hat{f}}_{i, j}\left(0\right)} = {{c} {{f}_{i, j}\left(0\right)}}\)
for an arbitrary constant \(c \neq 0\). We achieve violation of the
above inequality by choosing an \(f\) such that
\begin{align}\label{geqe04fcef18c889f0078e513a821114fdf}

        {2}   {\left| {f}_{1, 2} \right|}& \geq \left| {f}_{1, 1} + {f}_{2, 2} \right|
\end{align} at the origin and then constructing an appropriate
\(\hat{f}\) with a large enough \(c\). One possible choice of \(f\) is
\begin{align}\label{eqcdabda8eebc6c7937dbd3a8615d97efd}

        f\left(x, y\right)& = {\frac{1}{2}}   {\left(1 + {\left(1 + x\right)} ^ {2}\right)}   {\left(y + {y} ^ {3}\right)}
.\end{align}Let us now look at the case \(n > 2\). Given a non-constant
energy density \(h\), we will construct a Poisson bracket with
non-degenerate indefinite metric \(g\) such that the induced PDE is
non-hyperbolic on a set of non-zero measure. As above we will assume
\({{h}_{2}\left(0\right)} \neq {0}\). We will extend the constructions
above. Let the metric in flat coordinates be
\begin{align}\label{eqe5fd0ed7a9fcdd87e5e1138007036fe3}

        \hat{g}& = \begin{bmatrix}1&0&{0}_{1, \left[n-2\right]}\\0&-1&{0}_{1, \left[n-2\right]}\\{0}_{\left[n-2\right], 1}&{0}_{\left[n-2\right], 1}&{I}_{\left[n-2\right], \left[n-2\right]}\end{bmatrix}
\end{align} and consider coordinates
\begin{align}\label{eq55232315b16489d45aa5436083aef6b9}

        \begin{bmatrix}{u}^{1}_{}\\{u}^{2}_{}\\{u}^{3}_{}\\\vdots\end{bmatrix}& = \begin{bmatrix}{\hat{u}}^{1}_{}\\f\left({\hat{u}}^{1}_{}, {\hat{u}}^{2}_{}\right)\\{\hat{u}}^{3}_{}\\\vdots\end{bmatrix}
.\end{align} Then we get
\begin{align}\label{eq6765ee76fd1135adca49e8f92a876f6c}

        {\hat{h}}_{\hat{i}, \hat{j}}& = \begin{cases}\left({{f}_{1}} ^ {2}\right) {h}_{2, 2} + {h}_{2} {f}_{1, 1} + 2 {f}_{1} {h}_{1, 2} + {h}_{1, 1} & \text{ if } {\hat{i}} = {\hat{j}} = {1},\\
{h}_{2} {f}_{1, 2} + {f}_{1} {f}_{2} {h}_{2, 2} + {f}_{2} {h}_{1, 2} & \text{ if } {\hat{i}} = {1, 2},\\
\left({{f}_{2}} ^ {2}\right) {h}_{2, 2} + {h}_{2} {f}_{2, 2} & \text{ if } {\hat{i}} = {\hat{j}} = {2},\\
{h}_{1, \hat{j}} + {{h}_{2, \hat{j}}}   {{f}_{1}} & \text{ if } {{\text{min}(\hat{i}, \hat{j})} = {1}} \text{ and } {{\text{max}(\hat{i}, \hat{j})} \geq {3}},\\
{{h}_{2, \hat{j}}}   {{f}_{2}} & \text{ if } {{\text{min}(\hat{i}, \hat{j})} = {2}} \text{ and } {{\text{max}(\hat{i}, \hat{j})} \geq {3}},\\
{h}_{\hat{i}, \hat{j}} & \text{ else }\end{cases}
.\end{align} Note that for the \(f\) given above, at the origin we have
\({{f}_{1, 1}} = {{f}_{2, 2}} = {{f}_{1}} = {0}\) and
\({{f}_{2}} = {1}\), hence
\begin{align}\label{eq3b6a725df89dd1ae71235558df36d2e1}

        {\hat{h}}_{\hat{i}, \hat{j}}\left(0\right)& = \begin{cases}{h}_{1, 2}\left(0\right) + {{f}_{1, 2}\left(0\right)}   {{h}_{2}\left(0\right)} & \text{ if } {\hat{i}} = {1, 2},\\
{h}_{\hat{i}, \hat{j}}\left(0\right) & \text{ else }\end{cases}
.\end{align} For arbitrary \(n>2\), we of course do not have the luxury
of an explicit expression for the eigenvalues of a general matrix.
However, we may still construct a matrix such that we can ensure that it
has at least one eigenvalue pair with non-zero imaginary part. We will
use Gerschgorin's theorem (cf.~e.g.~\cite{stew1990}, chapter IV, theorem
2.1), ensuring that at least two Gerschgorin disks are each separated
from all others while not touching the real line.

Consider \begin{align}\label{eqea705081ac116b18031c6fa03bd6af7e}

        {\hat{g}}   {{\hat{\nabla}} ^ {2}}   {\hat{h}}& = \underbrace{\begin{bmatrix}{h}_{1, 1}&0&{h}_{1, 3}&\dots\\0&- {{h}_{2, 2}}&0&\dots\\{h}_{1, 3}&0&{h}_{3, 3}&\dots\\\vdots&\vdots&\vdots&\ddots\end{bmatrix}}_{A_\text{Symm}} + \underbrace{\begin{bmatrix}0&{\hat{h}}_{1, 2}&0&\dots\\- {{\hat{h}}_{1, 2}}&0&- {{h}_{2, 3}}&\dots\\0&{h}_{2, 3}&0&\dots\\\vdots&\vdots&\vdots&\ddots\end{bmatrix}}_{A_\text{Skew}}
\end{align} at the origin. The matrix \(A_\text{Skew}\) has non-zero
eigenvalues \begin{align}\label{eq86e05cf103e66ca73dc7c58dcb8ad2a8}

        {\lambda}_{1, 2}& = \pm{{i}   {\left| {\hat{h}}_{1, 2} + \sum_{j=3}^{n} {h}_{2, j} \right|}}
\end{align} and, by virtue of being antisymmetric, is unitarily
diagonalizable (over the complex numbers) with matrix \(Q\). After again
replacing \(f\) by \(\hat{f}\), we see that the only entry of
\({\hat{g}} {{\hat{\nabla}} ^ {2}} {\hat{h}}\) that depends on \(c\) is
\({\hat{h}}_{1, 2}\), which only appears in the antisymmetric second
part. We now apply Gerschgorin's theorem to
\({Q} {\hat{g}} {{\hat{\nabla}} ^ {2}} {\hat{h}} {Q^*}\).

The absolute values of all entries of \({Q} {A_\text{Symm}} {Q^*}\) can
be bounded by
\({{n} ^ {2}} {\left\lVert A_\text{Symm} \right\rVert_{\infty}}\),
independently of \(c\). \({Q} {A_\text{Skew}} {Q^*}\) on the other hand
will be diagonal and have as only non-zero entries two purely imaginary
diagonal entries, whose absolute values will \emph{grow} asymptotically
linearly with \(c\). Hence, the radii of all Gerschgorin disks can be
bounded independently of \(c\), as can the absolute values of all but
two centers of the Gerschgorin disks. For the centers whose absolute
values can not be bounded independently of \(c\), we can bound the
distance to \({\lambda}_{1, 2}\) independently of \(c\). However, as we
can make \(\left| {\lambda}_{1, 2} \right|\) arbitrarily large, this
means that we can choose \(c\) such that two Gerschgorin disks are
isolated from all others and do not touch the real line. This is
sufficient to conclude that, at the origin,
\({\hat{g}} {{\hat{\nabla}} ^ {2}} {\hat{h}}\) has at least one
eigenvalue pair with non-zero imaginary part, and hence the induced PDE
is non-hyperbolic at the origin and in a neighborhood around the origin.
This concludes the proof.

\end{proof}
\begin{remark}

\begin{itemize}
\tightlist
\item
  For \(n=1\) every first order quasilinear PDE is hyperbolic and
  Poisson.
\item
  For constant Hamiltonians the evolution equation is trivially zero.
\item
  For \(n>2\), the constructed metric need not be Lorentzian, but it
  simplifies the presentation.
\item
  If \(h_2(0) = 0\) we simply permute and shift the coordinate system.
\end{itemize}

\end{remark}
