% !TEX root = ../../main.tex
In this thesis we have seen

\begin{itemize}
\tightlist
\item
  a new class of discontinuous Galerkin methods that are tunable with
  respect to the dissipation they induce and
\item
  a first step towards a classification of hyperbolic partial
  differential equations that admit a GENERIC formulation.
\end{itemize}

\textbf{We have demonstrated superconvergence for the numerical
integrators and a superior qualitative behavior across a range of
dissipative regimes.} Numerical experiments for simple ODE and PDE
problems have revealed that the quantity of interest that should be
conserved is not necessarily the total energy of a system, but the
dissipation of the free energy. This has been demonstrated by superior
qualitative behavior of numerical integrators that approximate this
dissipation well as compared to numerical integrators that conserve the
total energy of the system but do not capture the dissipation of the
free energy. Another benefit of the proposed class of numerical
integrators is that while they interpolate between ``conservative'' and
``dissipative'' numerical integrators, in the limit of small step sizes
they converge towards a symmetric family of numerical integrators,
yielding one additional order of convergence in addition to the
superconvergence of the regular discontinuous Galerkin methods while
remaining A-stable.

While this new class of numerical integrators appears to be applicable
to ODE problems, the suitability for PDE problems remains uncertain. The
prototypical dissipative integrators, the regular discontinuous Galerkin
methods, are primarily applied to parabolic problems such as the heat
equation. Here, their L-stability leads to the appropriate damping of
very fast modes, almost independently of the temporal step size, which
is integral to capturing both the qualitative and quantitative behavior
in reasonable time. Non A-stable methods would require an excessive
number of timesteps to achieve comparable performance, while even
A-stable but non L-stable methods would not damp the very fast modes
enough. However, the necessity of L-stable methods for the heat equation
lies in the scaling of the eigenvalues with the spatial mesh size, which
is inversely quadratic.

In contrast, for the typical, allegedly reversible, hyperbolic problems,
the scaling of the eigenvalues with the spatial mesh size is only
inversely linear, giving rise to the famous hyperbolic CFL-condition and
enabling the efficient use of essentially explicit methods such as many
finite volume methods. Here, any globally implicit method will have a
hard time competing with efficient explicit or only locally implicit
methods. In addition to the unneccessary overhead induced by having to
solve a large system of equations, globally implicit methods generally
have the added drawback that they destroy one of the central properties
of hyperbolic PDEs, their finite propagation speed. With the reversible
limit of our family of methods being the symmetric discontinuous
Galerkin method, which is a globally implicit method, the adequacy of
our family of methods for purely ``reversible'' problems such as
hyperbolic PDEs is already in doubt. In addition, hyperbolic PDEs are
mostly only truly reversible in the smooth regime which is not reachable
by any discretization, meaning that no discretization of hyperbolic PDEs
should aim to truly capture the reversibility of the original problem.
These two aspects taken together suggest that the reversible limit is
not really relevant for most PDE applications.

However, even for the intermediate regime, when both reversible and
irreversible effects are relevant, our class of solvers may not be
adequate. A large class of intermediate problems still benefits from the
treatment with finite volume methods, namely hyperbolic PDEs with
relaxation terms but without an explicit Laplacian. Even in the smooth
regime, these are truly combinations of reversible and irreversible
dynamics, while in the non-smooth regime or equivalently if discretized,
they are irreversible, but mainly due to local effects so.

Thus, the most likely candidate of class of PDE problems for which our
class of methods is applicable appear to be problems with strong, global
dissipation. Whether or not our class of methods yields a benefit for
practical applications remains to be investigated.

For the second part of the thesis a first result was immediate:

\textbf{Except for one very special case, the irreversible part of
GENERIC systems may not influence their hyperbolicity because it cannot
add a first-order contribution.} This took a large part of the ambiguity
out of the problem of classifying hyperbolic GENERIC systems, as the
most flexible part of GENERIC was turned inconsequential. However, both
directions, determining whether a given hyperbolic PDE admits a GENERIC
formulation or deriving a priori criteria for GENERIC systems to be
hyperbolic or not, turned out to be quite difficult in general.

One of the problems in finding a GENERIC formulation of a given
hyperbolic PDE is that a priori the search space of potential Poisson
operators is very large and grows quite dramatically with the number of
unknown fields and the number of spatial dimensions but at the time is
severely and nonlinearly restricted. This problem could partially be
eliminated by building on existing classification results for
low-dimensional Poisson operators, but even this could only take us so
far. However, extending the original classification results for Poisson
operators was also not really a productive option, as the original
classification results stopped just before the number of canonical forms
started to explode. Thus, while a manual inspection of the known
canonical forms of low dimensional Poisson operators was tedious but
possible in reasonable time, this approach could not be extended
indefinitely or even very far. Still, we obtained some partial results
and some classes of GENERIC systems were identified that admit easily
checkable conditions for hyperbolicity.

However, the overall picture appears hopeless. There does not appear to
exist an efficient, deterministic workflow to decide whether or not a
given hyperbolic PDE admits a GENERIC formulation. While for many
physical systems a GENERIC formulation is known, for many, especially
higher-dimensional, hyperbolic PDEs the question of existence of a
GENERIC formulation remains open, with no general solution in sight.
While there have been several successful attempts at automating checking
whether a \emph{given} bracket is a Poisson bracket, so far there have
been none that automate \emph{finding} the Poisson bracket generating
some given system.

With the applicability of numerical GENERIC integrators to PDE problems
doubtful and without clear benefits provided by a GENERIC formulation
for practical applications, it remains to be seen whether further
research in this area is well invested.
