% !TEX root = ../../main.tex
\hypertarget{generic-flows}{%
\subsection{GENERIC flows}\label{generic-flows}}

MOVE THESE DEFINITIONS UP:

\begin{definition}[GENERIC
system]\label{definitionbd01a3e7db6ab387ad7866762ca6e85b} We call a
tuple consisting of

\begin{itemize}
\tightlist
\item
  a
  \hyperref[definition7b62cf90b137c29a2764ea5f708e87fd]{smooth manifold}
  \(\mathcal{U}\), called \emph{phase space},
\item
  a subspace of real-valued smooth functions
  \({\mathcal{F}} \subset {C^\infty\left(\mathcal{U}\right)}\), called
  \emph{space of functionals},
\item
  a
  \hyperref[definition1f3750646b440e2bdf6f38fdae2c1804]{Poisson operator}
  \(\mathcal{L}: {\mathcal{F}} \times {\mathcal{F}} \rightarrow \mathcal{F}\),
  called \emph{reversible operator}
\item
  a
  \hyperref[definitionc693645b194da9c5367ffa36fd3f4b45]{metric operator}
  \(\mathcal{M}: {\mathcal{F}} \times {\mathcal{F}} \rightarrow \mathcal{F}\),
  called \emph{irreversible operator}
\item
  an energy functional \({\mathcal{H}} \in {\mathcal{F}}\)
\item
  an entropy functional \({\mathcal{S}} \in {\mathcal{F}}\)
\end{itemize}

satisfying the \emph{mutual degeneracy requirement}
\begin{align}\label{eq1560d55465db055c3b0c59ce4884fcba}
\mathcal{L}\left(\mathcal{S}, \cdot\right) = \mathcal{M}\left(\mathcal{H}, \cdot\right) = 0\end{align}
a \emph{GENERIC} or \emph{metriplectic} system. \end{definition}

Both \emph{symplectic} and \emph{metric} systems are special cases of
\hyperref[definitionbd01a3e7db6ab387ad7866762ca6e85b]{GENERIC systems},
with trivial
\hyperref[definitionc693645b194da9c5367ffa36fd3f4b45]{metric operator}
and entropy functional \({\mathcal{S}} \in {\mathcal{F}}\) or trivial
\hyperref[definition1f3750646b440e2bdf6f38fdae2c1804]{Poisson operator}
and energy functional \({\mathcal{H}} \in {\mathcal{F}}\) respectively.

\begin{example}[GENERIC ODE
example]\label{example900feb8df6de48c8b1db19125fda8c36} The simplest
non-trivial GENERIC ODE example is the following:
\begin{subequations}\label{relationsfd2eb2731eb50601db9730382e3d9cc0}
\begin{align}\mathcal{U} &= {\mathbb{R}} ^ {3}\label{eq4a4fb85efa18f7501f881ad598b93078}
,\\
\mathcal{F} &= C^\infty\left(\mathcal{U}\right)\label{eq62b9b352019d1aa711238a672b736511}
,\\
\mathcal{L} &= \begin{bmatrix}0&1&0\\-1&0&0\\0&0&0\end{bmatrix}\label{eq3e450faa2818704b96156a17195ee85a}
,\\
\mathcal{M} &= \begin{bmatrix}0&0&0\\0&0&0\\0&0&1\end{bmatrix}\label{eq4cff0358b982fb81a6cff79fded50eb4}
,\\
\mathcal{H}\left(p, q, s\right) &= {\mathcal{H}\left(p, q, {s}_{0}\right)} \text{ for all } {{\left(p, q, s\right)} \in {\mathcal{U}}}\label{eq80f2629e11a131a7b9fa89f2cc26033b}
,\\
\mathcal{S}\left(p, q, s\right) &= {\mathcal{S}\left({p}_{0}, {q}_{0}, s\right)} \text{ for all } {{\left(p, q, s\right)} \in {\mathcal{U}}}\label{eq2b2f18c86564ff93df71ffc51e3afc63}
\end{align} \end{subequations} for some fixed
\({\left({p}_{0}, {q}_{0}, {s}_{0}\right)} \in {\mathcal{U}}\). The form
of the energy functional \({\mathcal{H}} \in {\mathcal{F}}\) and of the
entropy functional \({\mathcal{S}} \in {\mathcal{F}}\) ensure the
satisfaction of (\ref{eq1560d55465db055c3b0c59ce4884fcba}). The
evolution equation however decouples as the generated metriplectic
vector field \({\mathcal{V}} \in {{T} {\mathcal{U}}}\) can be written as
\begin{align}\label{eqecc14d23e84b815a155cdebf7ab5ed99}

        \mathcal{V}& = \underbrace{\begin{bmatrix}{\mathcal{H}}_{q}\\- {{\mathcal{H}}_{p}}\\0\end{bmatrix}}_{\text{Independent of {s}}} + \underbrace{\begin{bmatrix}0\\0\\{\mathcal{S}}_{s}\end{bmatrix}}_{\text{Independent of {p} and {q}}}
\end{align} \end{example}

\begin{example}[symplectic PDE
example]\label{example67c8df0e90a3d1c2fa8ca1ae3c514bb6} We want to
model this example on the \emph{inviscid Burgers'
equation}\begin{align}\label{eq6fd0eb7916db94c2c918f422bfaaa21f}

        {u}_{t} + {\partial}_{x}\left(\frac{{u} ^ {2}}{2}\right)& = 0
.\end{align} There is one obvious choice of
\hyperref[definitionbd01a3e7db6ab387ad7866762ca6e85b]{symplectic system}:
\begin{align}\label{and9c7d009d3d29e590d97400ad3477b24b}

        {\mathcal{L}} = {{\partial}_{x}}& \text{ and } {\mathcal{H}\left[u\right]} = {\int_{\mathbb{R}} \frac{{u} ^ {3}}{6} dx}
\end{align} but we also have the following general family of options for
the
\hyperref[definition1f3750646b440e2bdf6f38fdae2c1804]{Poisson operator}
and for the \emph{energy density} \(h\) reproducing the equation:
\begin{subequations}\label{relationsb4d0254d316609307b103de3ff671352}
\begin{align}\mathcal{L} &= {\partial}_{x}\left({f}   {\cdot}\right) + {f}   {{\partial}_{x}\left(\cdot\right)}\label{eq3eb757161fe16bb6674ea44e8bbf26dc}
,\\
\left(f, h\right) &\in \left\{{\left(f, h\right)} \in {{C^\infty\left(\mathbb{R}\right)} \times {C^\infty\left(\mathbb{R}\right)}}\middle|{{2}   {f\left(u\right)}   {{h}''\left(u\right)} + {{f}'\left(u\right)}   {{h}'\left(u\right)}} = {u}\right\}\label{containsf38335a36eb0c56bdb111188dbda4653}
\end{align} \end{subequations} which includes for example
\begin{align}\label{and4b5c5419bf7645726908d3789ed91b75}

        {f\left(u\right)} = {\frac{1}{2}}& \text{ and } {h\left(u\right)} = {\frac{{u} ^ {3}}{6}}
\end{align} The \emph{hydrodynamic}
\hyperref[definitionad39009c1a2696059717f066cd17a3ae]{Casimir functions}
are all smooth functions \({c} \in {C^\infty\left(\mathbb{R}\right)}\)
that are solutions of the ODE
\begin{align}\label{eq018989cb04ef27054e99969dca6e2757}

        {2}   {f\left(u\right)}   {{c}''\left(u\right)} + {{f}'\left(u\right)}   {{c}'\left(u\right)}& = 0
\end{align} which for example for
\({f\left(u\right)} = {\mathrm{const}}\) as above are all affine
functions. For the \emph{energy functional} \(\mathcal{H}\) to be
finite, an appropriate choice of the \emph{phase space} \(\mathcal{U}\)
is necessary. If for example \({h} = {\frac{{u} ^ {3}}{6}}\) as above,
one possible choice is
\({\mathcal{U}} = {C_0^\infty\left(\mathbb{R}\right)}\).\end{example}

\begin{example}[metric PDE
example]\label{example5faf150c3f2a8abf7245cedfa8889005} We want to
model this example on the \emph{heat
equation}\begin{align}\label{eq79db330218349b9b1d42b09e61f9d617}

        {u}_{t}& = {\underbrace{\varepsilon}_{>0}}   {{\partial}_{x, x}\left(u\right)}
.\end{align} There are two obvious choices of
\hyperref[definitionbd01a3e7db6ab387ad7866762ca6e85b]{metric systems}
reproducing the equation, either
\begin{align}\label{andbe2c3277bf91ae64675054f8f2ffd8c4}

        {\mathcal{M}} = {\varepsilon}& \text{ and } {\mathcal{S}\left[u\right]} = {- {\int_{\mathbb{R}} \frac{{{\partial}_{x}\left(u\right)} ^ {2}}{2} dx}}
\end{align} or \begin{align}\label{and3393d3ad9bea8e213a005020377d87dc}

        {\mathcal{M}} = {-{\varepsilon}   {{\partial}_{x, x}}}& \text{ and } {\mathcal{S}\left[u\right]} = {- {\int_{\mathbb{R}} \frac{{u} ^ {2}}{2} dx}}
\end{align} The only \emph{hydrodynamic}
\hyperref[definitionad39009c1a2696059717f066cd17a3ae]{Casimir functions}
are constant functions for the first option and affine functions for the
second option. Again, for the \emph{entropy functional} \(\mathcal{S}\)
to be finite, an appropriate choice of \emph{phase space}
\(\mathcal{U}\) is necessary. For the second case
\({\mathcal{U}} = {C_0^\infty\left(\mathbb{R}\right)}\) is again
appropriate, while being a bit restrictive for the first case, for which
we only need to require (for example)
\({\mathcal{U}} \subset {C^\infty\left(\mathbb{R}\right)}\) and
\({{\partial}_{x}\left(\mathcal{U}\right)} = {C_0^\infty\left(\mathbb{R}\right)}\).\end{example}

\begin{example}[GENERIC PDE
example]\label{example4c326138883e872bfbb323a0245e6f06} We want to
model this example on the \emph{viscous Burgers'
equation}\begin{align}\label{eqdc78efcd37f992f8332b27eff59ebc56}

        {u}_{t} + {\partial}_{x}\left(\frac{{u} ^ {2}}{2}\right)& = {\underbrace{\varepsilon}_{>0}}   {{\partial}_{x, x}\left(u\right)}
.\end{align} One would hope that we can simply combine example
\ref{example67c8df0e90a3d1c2fa8ca1ae3c514bb6} and example
\ref{example5faf150c3f2a8abf7245cedfa8889005}. This however turns out to
be impossible within the GENERIC framework. To see why consider first
the choice \begin{align}\label{andbe2c3277bf91ae64675054f8f2ffd8c4}

        {\mathcal{M}} = {\varepsilon}& \text{ and } {\mathcal{S}\left[u\right]} = {- {\int_{\mathbb{R}} \frac{{{\partial}_{x}\left(u\right)} ^ {2}}{2} dx}}
\end{align} As the \emph{energy functional} \(\mathcal{H}\) has to be
chosen from the set of
\hyperref[definitionad39009c1a2696059717f066cd17a3ae]{Casimir functions}
of the \emph{irreversible operator} \(\mathcal{M}\) it has to be
constant and hence \({{\mathcal{L}} {d} {\mathcal{H}}} = {0}\). Thus
this choice cannot be correct. Looking at
\begin{align}\label{and3393d3ad9bea8e213a005020377d87dc}

        {\mathcal{M}} = {-{\varepsilon}   {{\partial}_{x, x}}}& \text{ and } {\mathcal{S}\left[u\right]} = {- {\int_{\mathbb{R}} \frac{{u} ^ {2}}{2} dx}}
\end{align} however does not lead anywhere either. Again from the
requirement that the \emph{energy functional} \(\mathcal{H}\) has to be
a
\hyperref[definitionad39009c1a2696059717f066cd17a3ae]{Casimir function}
of the \emph{irreversible operator} \(\mathcal{M}\), we get that the
\emph{energy density} \(h\) has to be an affine function. As a
consequence of (\ref{relationsb4d0254d316609307b103de3ff671352}) we get
\begin{align}\label{eq6739bf5eb6057b85007a234cd3df1689}

        f\left(u\right)& = \frac{{u} ^ {2}}{{2}   {{h}'\left(u\right)}} + {c}_{f}
\end{align} with \({{c}_{f}} = {\mathrm{const}}\). But, now the
\emph{entropy functional} \(\mathcal{S}\) is no
\hyperref[definitionad39009c1a2696059717f066cd17a3ae]{Casimir function}
of the \emph{reversible operator} \(\mathcal{L}\), as can be verified by
plugging in the \emph{entropy density} \(s\) into
(\ref{eq018989cb04ef27054e99969dca6e2757}).

This however is not the end. Indeed, there is a way to shoehorn the
viscous Burgers' equation into a
\hyperref[definitionbd01a3e7db6ab387ad7866762ca6e85b]{GENERIC system}
and it involves the \emph{irreversible operator} \(\mathcal{M}\) and an
appropriate restriction of the \emph{phase space} \(\mathcal{U}\) and of
the \emph{space of functionals} \(\mathcal{F}\). First observe that
picking \begin{align}\label{and18e54f2908a09e934589fdd8b30b3a29}

        {\mathcal{M}} = {{\partial}_{x}\left(u\right)-{\varepsilon}   {{\partial}_{x, x}}}& \text{ and } {\mathcal{S}\left[u\right]} = {- {\int_{\mathbb{R}} \frac{{u} ^ {2}}{2} dx}}
\end{align} gives us the viscous Burgers' equation. But the
\emph{irreversible operator} \(\mathcal{M}\) is also required to be
non-negative for all members of the \emph{space of functionals}
\(\mathcal{F}\) and for all elements of the \emph{phase space}
\(\mathcal{U}\). To make the \emph{entropy functional} \(\mathcal{S}\)
finite, we pick \({\mathcal{U}} = {C_0^\infty\left(\mathbb{R}\right)}\)
as in example \ref{example67c8df0e90a3d1c2fa8ca1ae3c514bb6} and example
\ref{example5faf150c3f2a8abf7245cedfa8889005}. Next we note that for any
function \({v} = {v\left(u\left(x\right)\right)}\)
\begin{align}\label{eq83d0b5a1e04eb49972f49a9c0d436fbf}

        \mathcal{M}\left(v, v\right)& = \int_{\mathbb{R}} \left(-{\varepsilon}   {v\left(u\left(x\right)\right)}   {{\partial}_{x, x}\left(v\left(u\right)\right)\left(x\right)} + {{\partial}_{x}\left(u\right)\left(x\right)}   {{v\left(u\left(x\right)\right)} ^ {2}}\right) dx
\end{align} can be written as
\begin{align}\label{eq63fe6a3dcb3fdf9b3881bdf9617baa5b}

        \mathcal{M}\left(v, v\right)& = \int_{\mathbb{R}} \left({\varepsilon}   {{{\partial}_{x}\left(u\right)\left(x\right)} ^ {2}} + {\partial}_{x}\left(F\left(u\right)\right)\left(x\right)\right) dx
\end{align} with the first integrand always being positive and
\begin{align}\label{eq06811279b3ca16ea90b7c8637fa32908}

        F& = \int_{\left(0, u\right)} {v\left(\tau\right)} ^ {2} d\tau
.\end{align} Now, as we have \emph{chosen}
\({\mathcal{U}} = {C_0^\infty\left(\mathbb{R}\right)}\) the integral of
the second integrand vanishes for all
\({v} = {v\left(u\left(x\right)\right)}\)! Had we allowed \(v\) to
depend explicitly on the \emph{spatial variable} \(x\) we could not have
constructed the above \(F\), and indeed we could have constructed a
\(v\) such that the integral becomes negative. This then suggests that
we have to pick the set of \emph{hydrodynamic} functionals as our
\emph{space of functionals} \(\mathcal{F}\), as we can then ensure
non-negativity of the \emph{irreversible operator}
\(\mathcal{M}\).\end{example}
