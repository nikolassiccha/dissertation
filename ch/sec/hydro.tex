% !TEX root = ../../main.tex
\hypertarget{poisson-brackets-of-hydrodynamic-type}{%
\section{Poisson brackets of hydrodynamic
type}\label{poisson-brackets-of-hydrodynamic-type}}

\begin{definition}[Poisson
bracket]\label{definitionf46856abfef4afd236ad6b9849b6c354} A bilinear
bracket \begin{align}\label{mapsace8c49ad0764d6ff4cc654a2fcab664}
\left\{\cdot, \cdot\right\}: {V} \times {V} \rightarrow \mathbb{R}\end{align}
is called \emph{Poisson} if \(\left\{\cdot, \cdot\right\}\)

\begin{itemize}
\tightlist
\item
  is antisymmetric:
  \({\left\{F, G\right\}} = {- {\left\{G, F\right\}}}\),
\item
  is a derivation:
  \({\left\{{F} {G}, H\right\}} = {{F} {\left\{G, H\right\}} + {G} {\left\{F, H\right\}}}\)
  and
\item
  satisfies the Jacobi-identity:
  \({{\left\{F, \left\{G, H\right\}\right\}}_{\circlearrowright F, G, H}} = {0}\)
\end{itemize}

\end{definition}

In \cite{Dubr1983a} and \cite{Dubr1983b} Dubrovin and Novikov introduced
Poisson brackets and Hamiltonians of \emph{hydrodynamic type}:
\begin{definition}[hydrodynamic]\label{definitionae6a92d96b87645337ec28902b86eb8f}
A \hyperref[definitionf46856abfef4afd236ad6b9849b6c354]{Poisson bracket}
\(\left\{\cdot, \cdot\right\}\) is called \emph{hydrodynamic} if it
satisfies \begin{align}\label{eq76b9bc6de10a2f65da35d1ae66f99938}

        \left\{{u}^{i}_{}\left(x\right), {u}^{j}_{}\left(y\right)\right\}& = {{g}^{\mu, i, j}_{}\left(u\left(x\right)\right)}   {{\left[{\delta}'\right]}_{\mu}\left(x- {y}\right)} + {{u}^{k}_{\mu}}   {{b}^{\mu, i, j}_{k}\left(u\left(x\right)\right)}   {\delta\left(x- {y}\right)}
.\end{align}\end{definition}

\begin{definition}[hydrodynamic]\label{definition321e5ace79f411ee1af33d373ba1babf}
A functional (or Hamiltonian)
\begin{align}\label{maps88bf98b4109affa6533eeb1a189beb8f}
H: C^\infty\left(\mathcal{M}^d; \mathcal{U}^n\right) \rightarrow \mathbb{R}\end{align}
is called \emph{hydrodynamic} if it is of the form
\begin{align}\label{eqd6887283c2d2d388c4676402932d8d58}

        {H}   {\left[u\right]}& = \int \left(h\left(u\left(x\right)\right)\right)\, dx
\end{align} for some energy density
\({h} \in {C^\infty\left(\mathcal{U}^n\right)}\).\end{definition}

By requiring that brackets and Hamiltonians do not depend themselves on
spatial derivatives of the unknown field variables
\({u} \in {C^\infty\left(\mathcal{M}^d; \mathcal{U}^n\right)}\), we
ensure that the resulting evolution equation will be a
\hyperref[definition261ec0b449061e6157ca265aa497244e]{system of quasilinear first-order PDEs}.

For any two Functionals
\({F\left(u\right)} = {\int \left(f\left(u\left(x\right)\right)\right)\, dx}\)
and
\({H\left(u\right)} = {\int \left(h\left(u\left(x\right)\right)\right)\, dx}\)
we get \begin{align}\label{eqa403122cf9380aaf5ee1d81a0df8cff9}

        \left\{F, H\right\}& = \int {\frac{\partial f}{\partial {u}^{i}_{}}}   {{L}^{i, j}_{}}   {\frac{\partial h}{\partial {u}^{j}_{}}}\, dx
\end{align}

with \emph{differential operator}
\({{L}^{i, j}_{}} = {{{g}^{\mu, i, j}_{}} {\frac{\partial }{\partial {x}_{\mu}}} + {{b}^{\mu, i, j}_{k}} {{u}^{k}_{\mu}}}\).
Finally, given a Poisson bracket and a hydrodynamic Hamiltonian
functional \(H\) we get the \emph{evolution
equation}\begin{align}\label{eqeb1b92f04d3af4b11ed8426d9769904d}

        {u}^{i}_{t}& = {{{L}^{i, j}_{}}   {{h}_{j}}} = {{\left({{g}^{\mu, i, j}_{}}   {{h}_{j, k}} + {{b}^{\mu, i, j}_{k}}   {{h}_{j}}\right)}   {{u}^{k}_{\mu}}}
,\end{align} where we can identify the
\emph{\((1,1)\)-tensors}\begin{align}\label{eqc86cfb5828a8055f9b3f92d459d8e2c2}

        {A}^{\mu, i}_{k}& = {{g}^{\mu, i, j}_{}}   {{h}_{j, k}} + {{b}^{\mu, i, j}_{k}}   {{h}_{j}}
.\end{align}

For the above bracket to be Poisson, it has to satisfy the \emph{Jacobi
identity}\begin{align}\label{eq603b4cee5f85bb156fe58b0da4c872df}

        {\left\{F, \left\{G, H\right\}\right\}}_{\circlearrowright F, G, H}& = 0
\end{align} for all hydrodynamic Functionals \(F, G, H\) which, while
innocent looking, translates to (see \cite{Mokh1998}, Lemma 2.1):
\begin{subequations}\label{relationsc8f5eee804ce8db284850ef06a183d08}
\begin{align}0 &= {\left[{g}^{\mu, i, j}_{}\right]}_{\left[i, j\right]}\label{eq77dcea0bcc4718d2e4f2080066c60085}
,\\
0 &= \frac{\partial {g}^{\mu, i, j}_{}}{\partial {u}^{k}_{}}{-2}   {{\left[{b}^{\mu, i, j}_{k}\right]}_{\left(i, j\right)}}\label{eq9e370ffa3b211328bad202114076e71f}
,\\
0 &= {\left[{\left[{{g}^{\mu, i, l}_{}}   {{b}^{\nu, j, k}_{l}}\right]}_{\left[i, j\right]}\right]}_{\left(\mu, \nu\right)}\label{eq5a6ab21c4a385384d7d6a2a7bf4949e6}
,\\
0 &= {\left[{\left[{{g}^{\mu, i, l}_{}}   {{b}^{\nu, j, k}_{l}}\right]}_{\left[\left(i, \mu\right), \left(j, \nu\right)\right]}\right]}_{\circlearrowright i, j, k}\label{eqd35817307a08e212cd12c45c449b66ab}
,\\
0 &= {{{g}^{\mu, i, l}_{}}   {{\left[\frac{\partial {b}^{\nu, j, k}_{l}}{\partial {u}^{m}_{}}\right]}_{\left[m, l\right]}} + {\left[{{b}^{\mu, i, j}_{l}}   {{b}^{\nu, l, k}_{m}}\right]}_{\left[j, k\right]}}_{\left(\mu, \nu\right)}\label{eq80c30d5321fd8e883d47354af2fbb022}
,\\
0 &= {{{g}^{\mu, i, l}_{}}   {\frac{\partial {b}^{\nu, j, k}_{m}}{\partial {u}^{l}_{}}}-{{b}^{\nu, i, j}_{l}}   {{b}^{\mu, l, k}_{m}}-{{b}^{\nu, i, k}_{l}}   {{b}^{\mu, j, l}_{m}}}_{\left[\left(i, \mu\right), \left(j, \nu\right)\right]}\label{eqc72536ef2de79c086154a3bbf20a53bf}
,\\
0 &= {\frac{\partial}{\partial {u}^{n}_{}}\left({{g}^{\mu, i, l}_{}}   {{\left[\frac{\partial {b}^{\nu, j, k}_{l}}{\partial {u}^{m}_{}}\right]}_{\left[m, l\right]}}\right) + \frac{\partial}{\partial {u}^{n}_{}}\left({\left[{{b}^{\mu, i, j}_{l}}   {{b}^{\nu, l, k}_{m}}\right]}_{\left[j, k\right]}\right)- {{\left[{{b}^{\mu, l, i}_{m}}   {{\left[\frac{\partial {b}^{\nu, j, k}_{l}}{\partial {u}^{m}_{}}\right]}_{\left[m, l\right]}}\right]}_{\circlearrowright i, j, k}}}_{\left(\left(\mu, m\right), \left(\nu, n\right)\right)}\label{eqa472dfbfccf15353bd752945e19f5ec6}
.\end{align}\end{subequations}

\begin{definition}[Poisson
pair]\label{definition5ec290680da3509ccaec9242c3323ce2} A pair
\(\left({g}^{\mu}_{}, {b}^{\mu}_{}\right)\) is called \emph{Poisson}, if
it induces a
\hyperref[definitionae6a92d96b87645337ec28902b86eb8f]{hydrodynamic}
\hyperref[definitionf46856abfef4afd236ad6b9849b6c354]{Poisson bracket}.\end{definition}

\begin{definition}[Poisson]\label{definition1f2a57e9a0dde2fd2636fa1aff696c09}
A collection of metrics \({g}^{\mu}_{}\) is called \emph{Poisson}, if
there exists a collection \({b}^{\mu}_{}\) such that the pair
\(\left(g, b\right)\) is a
\hyperref[definition5ec290680da3509ccaec9242c3323ce2]{Poisson pair}.\end{definition}

\hypertarget{with-nondegenerate-metric}{%
\subsection{with nondegenerate metric}\label{with-nondegenerate-metric}}

\begin{definition}[non-degenerate]\label{definition33ed304f5ae1446b07bab107437a5e76}
A metric \(g\left(u\right)\) is called \emph{non-degenerate} if
\({\mathrm{det}\left(g\left(u\right)\right)} \neq {0}\)
everywhere.\end{definition}

\begin{definition}[non-degenerate]\label{definitioncd88069e17d1d040a8501b599f525907}
A Poisson bracket
(\ref{equationmultidimensional-poisson-brackets-of-hydrodynamic-type})
is called \emph{non-degenerate} if all metrics
\({g}^{\mu}_{}\left(u\right)\) are
\hyperref[definition33ed304f5ae1446b07bab107437a5e76]{non-degenerate}.\end{definition}

\hypertarget{in-one-spatial-dimension}{%
\subsubsection{in one spatial
dimension}\label{in-one-spatial-dimension}}

The main result, due to Dubrovin and Novikov (\cite{Dubr1983b}, theorem
1) and most succinctly stated in \cite{Mokh2000}, is the following
\begin{theorem}\label{theoremc7e81360d05b8ed6ea9416cd6ce0fc23} Let
\({\mathrm{det}\left(g\left(u\right)\right)} \neq {0}\) and
\({d} = {1}\). The bracket
(\ref{equationmultidimensional-poisson-brackets-of-hydrodynamic-type})
is a
\hyperref[definitionf46856abfef4afd236ad6b9849b6c354]{Poisson bracket}
if and only if \(g\left(u\right)\) is an arbitrary flat
\hyperref[definitioncd456d6a8761ef7d660ed99f1306cda6]{pseudo-Riemannian}
metric and
\({{b}^{i, j}_{k}\left(u\right)} = {-{{g}^{i, l}_{}\left(u\right)} {{\Gamma}^{j}_{l, k}\left(u\right)}}\),
where \({\Gamma}^{j}_{l, k}\left(u\right)\) is the Riemannian connection
generated by the metric \(g\left(u\right)\).\end{theorem}

\begin{proof}Stated without explicit proof in \cite{Dubr1983b}. Can be
proved by `direct verification'.\end{proof}

Due to flatness of the metric, we immediately get existence of (local)
coordinates \(u\) where
\({{g}^{i, j}_{}\left(u\right)} = {{{e}_{i}} {{\delta}^{i}_{j}}}\) with
\({{e}_{i}} = {\pm{1}}\) and \({b\left(u\right)} = {0}\). Hence
\emph{all} non-degenerate one-dimensional Poisson brackets are
classified by the signature of their metric.

\hypertarget{in-multiple-spatial-dimensions}{%
\subsubsection{in multiple spatial
dimensions}\label{in-multiple-spatial-dimensions}}

A generalization of theorem
\ref{theoremc7e81360d05b8ed6ea9416cd6ce0fc23}, summarized in
\cite{Mokh2006}, uses heavily the notion of \emph{compatible metrics}
developed in \cite{Mokh2000}:

This definition naturally mirrors the definition of \emph{compatible
Poisson brackets}, of which linear combinations are again required to be
Poisson brackets (see e.g.~\cite{Fera2001} or \cite{Mokh2002b}). Indeed,
the following result is immediate:
\begin{theorem}\label{theoremc7bf90423eabb860301d4bb510a62824} The
metrics \({g}^{\mu, i, j}_{}\) defining a multidimensional Poisson
bracket of the form
(\ref{equationmultidimensional-poisson-brackets-of-hydrodynamic-type})
are compatible.\end{theorem}

\begin{proof}See \cite{Mokh1998}, chapter II.\end{proof}

However, this is far from enough. Indeed, again from \cite{Mokh1998},
theorem 2.1:
\begin{theorem}\label{theorem73acfd9255502a783d4d0dc77787907f} Flat
non-degenerate metrics \({g}^{\mu, i, j}_{}\left(u\right)\) and the
Riemannian connections \({\Gamma}^{\mu, i}_{j, k}\) defined by these
metrics (the Levi-Civita connections) generate a Poisson structure
(\ref{equationmultidimensional-poisson-brackets-of-hydrodynamic-type})
if and only if the tensors
\({{T}^{\mu, \nu, i}_{j, k}} = {{\Gamma}^{\nu, i}_{j, k}- {{\Gamma}^{\mu, i}_{j, k}}}\)
satisfy the following relations (no summation over \(\mu\)):
\begin{subequations}\label{relations063a4d45e1af70d635671e05ad4fc2e9}
\begin{align}{\left[{T}^{\mu, \nu, i, j, k}_{}\right]}_{\left[i, k\right]} &= 0\label{eq6cee0511ee1fadd1d83c0af6a308b965}
,\\
{\left[{T}^{\mu, \nu, i}_{j, k}\right]}_{\circlearrowright i, j, k} &= 0\label{eq89542358bebcc96a5b1cce867b59d150}
,\\
{\left[{{T}^{\mu, \nu, i, j, m}_{}}   {{T}^{\mu, \nu, k}_{m, l}}\right]}_{\left[k, l\right]} &= 0\label{eq6aff5e4b86b611427878880371f2c9e3}
,\\
{{\nabla}^{\mu}_{l}}   {{T}^{\mu, \nu, i, j, k}_{}} &= 0\label{eqcd6d1467bbb99a9af131ebfa0d94d1f5}
\end{align} \end{subequations} where
\({{T}^{\mu, \nu, i, j, k}_{}} = {-{{g}^{\nu, k, m}_{}} {{T}^{\mu, \nu, i, j}_{m}}} = {-{{g}^{\nu, k, m}_{}} {{g}^{\mu, i, n}_{}} {{T}^{\mu, \nu, j}_{m, n}}}\)
and \({\nabla}^{\mu}_{l}\) is the covariant derivative associated with
\({g}^{\mu}_{}\).\end{theorem}

Similarly to the case of one spatial dimension, there are
multi-dimensional hydrodynamic Poisson brackets that can be reduced to
\emph{constant} form. However, contrary to the one-dimensional case,
non-degeneracy of the associated metric(s) is not a sufficient
condition. Instead, we need additionally that the \emph{obstruction
tensors} \({T}^{\mu, \nu, i}_{j, k}\) are identically zero. We will need
the concept of \emph{non-singular} pairs of metrics:

Finally we can state
\begin{theorem}\label{theorem79b430fa9e9a467a22414682163dc927} If for a
non-degenerate multidimensional Poisson bracket
(\ref{equationmultidimensional-poisson-brackets-of-hydrodynamic-type})
one of the metrics \({g}^{\mu, i, j}_{}\left(u\right)\) forms
non-singular pairs with all the remaining metrics of the bracket, then
this Poisson bracket can be reduced to constant form by a local change
of coordinates.\end{theorem}

And for all other (non-degenerate) Poisson brackets we have
\begin{theorem}\label{theorem04cfea04403adcd8ace555c3ea19512f} Consider
a Poisson bracket
(\ref{equationmultidimensional-poisson-brackets-of-hydrodynamic-type}).
If the metric \({g}^{1, i, j}_{}\) is non-degenerate, then in flat
coordinates \(u\) where \({g}^{1, i, j}_{}\left(u\right)\) is constant
and thus \({{b}^{1, i, j}_{k}} = {0}\), every other metric
\({g}^{\mu, i, j}_{}\) is \emph{linear}.\end{theorem}

\begin{proof}See \cite{Dubr1983a}, theorem 1 and \cite{Reyn2010},
proposition 2.15.\end{proof}

\begin{remark}Non-degeneracy of \emph{all} metrics is not
required.\end{remark}

\hypertarget{with-degenerate-metric}{%
\subsection{with degenerate metric}\label{with-degenerate-metric}}

\hypertarget{in-one-spatial-dimension-1}{%
\subsubsection{in one spatial
dimension}\label{in-one-spatial-dimension-1}}

Following the classification of non-degenerate one-dimensional Poisson
brackets
(\ref{equationmultidimensional-poisson-brackets-of-hydrodynamic-type})
in \cite{Dubr1983a}, Grinberg provided answers to the question of
classification of \emph{degenerate} one-dimensional Poisson brackets
with metric \(g\left(u\right)\) of constant rank in \cite{Grin1985}.
However, proofs were lacking in the literature until the publication of
\cite{Bogo2007b}. We will summarize the main statements.

For now, we do not require the \emph{(2,0)-tensor field} \(g\) to be
associated with a Poisson bracket. First, let us partition the
\emph{\(n\)-dimensional target manifold} \(\mathcal{U}^n\) by defining
the \emph{subsets of rank
\(k\)}\begin{align}\label{subset09c6ee595ee4e3cdf9ad3a17324c1642}

        {{\mathcal{O}}_{k}} = {\left\{{u} \in {\mathcal{U}^n}\middle|{\mathrm{rank}\left(g\left(u\right)\right)} = {k}\right\}}& \subset \mathcal{U}^n
.\end{align} Next, to depart from the non-degenerate case, let us
require that at least one \({\mathcal{O}}_{m}\) is nonempty and open for
some \(m < n\). Now, in slight disagreement with \cite{Bogo2007b}, we
have:

\begin{proposition}\label{proposition2ec7a4f8eadeb4402617663b8327159f}
Consider a \emph{(2,0)-tensor field} \(g\) with nonempty, open set
\({\mathcal{O}}_{m}\) as in \ref{definitionsubsets-of-rank-$k$}. Then

\begin{itemize}
\tightlist
\item
  \(g\) being smooth and
  \hyperref[definition1f2a57e9a0dde2fd2636fa1aff696c09]{Poisson} does
  not imply
  \({\overline{\cup_{k=0}^{n-1} {\mathcal{O}}_{k}}} = {\mathcal{U}^n}\),
\item
  if \(g\) is analytic, then \({\mathcal{O}}_{m}\) is dense in
  \(\mathcal{U}^n\),
  i.e.~\({\overline{{\mathcal{O}}_{m}}} = {\mathcal{U}^n}\).
\end{itemize}

\end{proposition}

\begin{proof}For the first point we will construct a counterexample.
All functions used in this part will be smooth. Consider a diagonal
metric \begin{align}\label{eqeaa870fe1dd28cc0b117f596e87cfac3}

        g\left(u\right)& = \begin{bmatrix}{g}^{1}_{}\left(u\right)&0\\0&{g}^{2}_{}\left(u\right)\end{bmatrix}
\end{align} on the target manifold
\({\mathcal{U}^n} = {{\mathbb{R}} ^ {2}}\). Together with
\begin{align}\label{eqe47b0c166b7e48229e745618d618347b}
b\left(u\right) = {\frac{1}{2}}   {{\nabla}_{u}}   {\begin{bmatrix}{g}^{1}_{}\left(u\right)&{b}^{1, 2}_{}\left(u\right)\\- {{b}^{1, 2}_{}\left(u\right)}&{g}^{2}_{}\left(u\right)\end{bmatrix}}\end{align}
this induces a hydrodynamic Poisson bracket if
\begin{align}\label{eq43bdcbf039e141b55d4103a20be3faa6}
{g}^{2}_{} = {b}^{1, 2}_{} = 0\end{align} or if
\begin{align}\label{eqa2fe8c287e5d37e66fab98c8a2b2ad64}
- {{g}^{2}_{}\left(u\right)} = {g}^{1}_{}\left(u\right) = {b}^{1, 2}_{}\left(u\right) = {g}^{0}_{}\left({u}^{1}_{} + {u}^{2}_{}\right)\end{align}
for all \({u} \in {\mathcal{U}^n}\), see
\href{https://colab.research.google.com/github/nsiccha/hyperbolic_generic/blob/master/nb/hydro_smooth_counterexample.ipynb?flush_cache=true}{\texttt{hydro\_smooth\_counterexample.ipynb}}.
We may stitch together the above families of Poisson brackets, as long
as they are separated from each other, as for example as follows:
\begin{subequations}\label{relations1750765a52116280b4fed8c3df5cc062}
\begin{align}{g}^{1}_{}\left(u\right) &= {g}^{0}_{}\left({u}^{1}_{} + {u}^{2}_{}\right)\label{eq54fb0621532ac558563fc9309bad265c}
,\\
- {{g}^{2}_{}\left(u\right)} &= \begin{cases}{g}^{1}_{}\left(u\right) & \text{ if } {{u}^{1}_{} + {u}^{2}_{}} < {- {\varepsilon}},\\
0 & \text{ if } {{u}^{1}_{} + {u}^{2}_{}} \geq {- {\varepsilon}}\end{cases}\label{eq9d4522418d13796a33736e4a76b93851}
,\\
{g}^{0}_{}\left(x\right) &= \begin{cases}{} \neq {0} & \text{ if } {\left| x \right|} > {\varepsilon},\\
0 & \text{ if } {\left| x \right|} \leq {\varepsilon}\end{cases}\label{eq8f44e0ec45a58fa0524ce1fc47632573}
,\end{align}\end{subequations} with \({\varepsilon} > {0}\) and
\({g}^{0}_{}\) constructed appropriately. We now have
\begin{align}\label{eq6e66a5404daf2e6b17788e1b393d0f6e}

        \mathrm{rank}\left(g\right)& = \begin{cases}2 & \text{ if } {{u}^{1}_{} + {u}^{2}_{}} < {- {\varepsilon}},\\
0 & \text{ if } {\left| {u}^{1}_{} + {u}^{2}_{} \right|} \leq {\varepsilon},\\
1 & \text{ if } {{u}^{1}_{} + {u}^{2}_{}} > {\varepsilon}\end{cases}
\end{align} and thus
\begin{align}\label{eqa8ac77a17056bde4715c0e42154204ad}

        \overline{\cup_{k=0}^{n-1} {\mathcal{O}}_{k}}& = {\left\{{{u}^{1}_{}, {u}^{2}_{}} \in {\mathcal{U}^n}\middle|{\left({u}^{1}_{} + {u}^{2}_{}\right)} \in {\left[- {\varepsilon}, \infty\right)}\right\}} \neq {\mathcal{U}^n}
,\end{align} which completes the proof of the first part.

For the second part we use the following factoid (see
e.g.~\cite{Hogb2013}, chapter 2.4, fact 25):

\begin{quote}
A square matrix \(A\) has rank \(k\) if and only if \(A\) has a
nonsingular \(k\times k\) submatrix, and every \((k+1)\times (k+1)\)
submatrix of \(A\) is singular.
\end{quote}

As the determinant of a matrix with analytic coefficients is itself
analytic, square submatrices of \(g\) that are singular on some open set
\({{\mathcal{O}}_{m}} \subset {\mathcal{U}^n}\) stay singular on the
whole of the \emph{\(n\)-dimensional target manifold} \(\mathcal{U}^n\).
For the same reason, square submatrices of \(g\) that are nonsingular on
some open set \({{\mathcal{O}}_{m}} \subset {\mathcal{U}^n}\) can not be
singular on all of any open set. Given any \({x} \in {\mathcal{U}^n}\),
this includes \emph{any} open set
\({{\mathcal{O}}'} \subset {\mathcal{U}^n}\) containing \(x\). Thus,
every neighborhood of every point \({x} \in {\mathcal{U}^n}\) contains
at least one point from \({\mathcal{O}}_{m}\), which is thus dense in
\(\mathcal{U}^n\), i.e.~its closure equals \(\mathcal{U}^n\) (see
e.g.~\cite{Bour1995}, chapter 1, definition 10).\end{proof}

\begin{remark}The phrasing in \cite{Bogo2007b} is slightly ambiguous.
On page 540 he claims that ``if the functions
\({g}^{i, j}_{}\left(u\right)\) are smooth, [\ldots] the [target
manifold \(\mathcal{U}^n\)] is the closure of the union
\(\cup_{k=0}^{n-1} {\mathcal{O}}_{k}\).'\,'

Two sentences before, he talks about ``any degenerate (2,0)-tensor
\({g}^{i, j}_{}\left(u\right)\)'\,', suggesting that the requirement for
\(g\) to be Poisson can be dropped. The following theorem hower relies
on \(g\) being Poisson, so we must interpret the whole section to carry
this requirement implicitly. If indeed \(g\) were not required to be
Poisson, an even simpler counterexample could have been constructed, as
we are not constrained by the considerable requirements induced by the
Jacobi identity. Next, the \emph{exact} meaning of \(g\) being
degenerate has not been stated in \cite{Bogo2007b}. If it were to mean
that \({\mathcal{O}}_{n}\) is empty, the above statement would be
trivially true, but then there would be no need of closing the union.
The statement closest to a definition of degeneracy can be found on page
539:

\begin{quote}
In Section 2, we disclose the invariant meaning of the Poisson brackets
for any values of \(k\) with degenerate (2,0)-tensor
\({g}^{i, j}_{}\left(u\right)\) that has a constant rank
\({{{g}^{i, j}_{}} = {m}} < {n}\) in an open domain
\({\mathcal{O}}_{m}\).
\end{quote}

Note that the phrasing does not imply that \({\mathcal{O}}_{m}\) is
nonempty, that \({\mathcal{O}}_{m}\) is defined as in
\ref{definitionsubsets-of-rank-$k$}, or that \({\mathcal{O}}_{n}\) is
empty. Again, if the requirement were not that at least one
\({\mathcal{O}}_{m}\) as defined in \ref{definitionsubsets-of-rank-$k$}
with \(m < n\) is open (and nonempty), a simpler counterexample with a
metric \(g\) that is Poisson could have been constructed.

Finally, at the end of Section 1, page 540 in \cite{Bogo2007b} a further
constraint is mentioned, namely \(1 < m < n\). This constraint only
seems to apply to Corollary 1 in \cite{Bogo2007b}, where it is
explicitly invoked. Even if it were implied in the contested statement,
we could simply construct a \(\hat{g}\) which is block-diagonal, with
the \(g\) from the proof as one block and another matrix with arbitrary
rank as the other block.

Cf. figure \ref{fig:partitions_fig} for all constructions. For \(k < m\)
we have \begin{align}\label{subset93fa72c6d0373db3a33af0bb9465eb34}

        {\overline{{\mathcal{O}}_{k}}} \cap {\overline{{\mathcal{O}}_{m}}}& \subset \cup_{l=0}^{k} {\mathcal{O}}_{l}
\end{align} and thus

\begin{itemize}
\tightlist
\item
  for the left figure \({\mathcal{O}}_{1}\) is open but \(g\) is not
  Poisson,
\item
  for the middle figure \(g\) is Poisson but \({\mathcal{O}}_{1}\) is
  not open as it shares a boundary with \({\mathcal{O}}_{2}\),
\item
  for the right figure, \(g\) is Poisson and \({\mathcal{O}}_{1}\) is
  open
\item
  for all figures, \({\mathcal{O}}_{0}\) is not open and
  \({\mathcal{O}}_{2}\) is open.\end{itemize}\end{remark}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{partitions_fig.pdf}
    \caption[Constructions for PROPOSITION]{Constructions for PROPOSITION. Colors and numbers indicate {Rank(g(u))}.}
    \label{fig:partitions_fig}
\end{figure}

\begin{theorem}\label{theoremd5ac6fbf6edf14dc41cce6f5dd3b972c} Let
\({L}_{u}\) and \({\mathcal{O}}_{k}\) be as in
\ref{definitiondistribution} and \ref{definitionsubsets-of-rank-$k$}.
Then

\begin{itemize}
\tightlist
\item
  An invariant non-degenerate metric is defined on the distribution
  \({L}_{u}\),
\item
  The distribution \({L}_{u}\) is invariant under any \((1,1)\)-tensor
  \({A}^{i}_{j}\left(u\right)\) (\ref{definition$(1,1)$-tensors}) and
  operator \({b}^{i, j}_{k}\left(u\right)\) for
  \({j} = {\mathrm{const}}\),
\item
  In the open domain \({\mathcal{O}}_{m}\), the distribution \({L}_{u}\)
  is involutive and defines an \(m\)-dimensional foliation
  \({\mathcal{F}}_{m}\),
\item
  The metric on the leaves of \({\mathcal{F}}_{m}\) is flat.
\end{itemize}

\end{theorem}

\begin{proof}See \cite{Bogo2007b}, theorem 1.\end{proof}
